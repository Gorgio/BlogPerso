<?php
\Core\System\System::init();
\Core\Globals\Get::init();
\Core\Globals\Post::init();
\Core\Globals\Cookies::init();
\Core\Globals\Server::init();

require_once 'config' . DS . 'config.php';

\Core\System\System::config();
