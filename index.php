<?php
const ROOT = __DIR__;
const DS = '/';

require_once 'vendor' . DS .'autoload.php';
require_once 'bootstrap.php';

App\Sessions\Sessions::init();

$dispatcher = FastRoute\simpleDispatcher(
    function (FastRoute\RouteCollector $r) {
        $r->get('/connect[/]', 'User#connectForm');
        $r->post('/connect[/]', 'User#connect');
        $r->get('/logout[/]', 'User#disconnect');

        $r->get('/', 'Homepage#home');

        $r->addGroup(
            '/articles',
            function (\FastRoute\RouteCollector $r) {
                $r->get('/{id:[0-9]+}[-{slug:[a-z\-0-9]+}]', 'Article#show');
                $r->get('/page/{page:[0-9]+}[/]', 'Article#listArticles');
                $r->get('/', 'Article#listArticles');
            }
        );

        // Admin path routes.
        $r->addGroup(
            '/admin',
            function (\FastRoute\RouteCollector $r) {
                $r->get('/articles/add[/]', 'Article#addArticle');
                $r->get('/articles/edit/{id:[0-9]+}[/]', 'Article#editArticle');
                $r->get('/articles/page/{page:[0-9]+}[/]', 'Article#listArticlesForAdmin');
                $r->get('/articles/', 'Article#listArticlesForAdmin');

                $r->get('/logs/{name:[a-zA-Z0-9/_.]+}[/]', 'Logs#show');
                $r->get('/logs/', 'Logs#listLogs');

                // Need to find a fix for this or understand how to make it work with FastRoute.
                $r->get('/users/edit/{id:[\w-]{' . \App\User\User::ID_LENGTH . '}}[/]', 'User#editUser');
                $r->get('/users/add[/]', 'User#addUser');
                $r->get('/users/page/{page:[0-9]+}[/]', 'User#listUsersForAdmin');
                $r->get('/users/', 'User#listUsersForAdmin');

                $r->get('/', 'Admin#dashboard');

                // POST method routes.
                $r->post('/articles/add[/]', 'Article#addArticleToDB');
                $r->post('/articles/edit[/]', 'Article#editArticleToDB');

                $r->post('/categories/add[/]', 'Category#add');
                $r->post('/categories/edit[/]', 'Category#edit');
                $r->post('/categories/delete[/]', 'Category#delete');

                $r->post('/ranks/add[/]', 'Rank#add');
                $r->post('/ranks/edit[/]', 'Rank#edit');
                $r->post('/ranks/delete[/]', 'Rank#delete');

                $r->post('/users/add[/]', 'User#addUserToDB');
                $r->post('/users/edit[/]', 'User#editUserToDB');
            }
        );
    }
);

// Fetch method and URI from somewhere.
$server = filter_input_array(INPUT_SERVER);
$httpMethod = $server['REQUEST_METHOD'];
$uri = $server['REQUEST_URI'];
// Strip query string (?foo=bar) and decode URI.
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = ltrim($uri, \Core\System\System::$webRoot);
$uri = DS . $uri;
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        echo 'HTTP Error 404 Not Found';
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        echo 'HTTP Error 405 Method Not Allowed';
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        // ... call $handler with $vars
        $params = explode("#", $handler);
        $controller = "App\\$params[0]\\" . $params[0] . "Controller";

        if ($httpMethod === 'POST') {
            array_unshift($vars, filter_input_array(INPUT_POST));
        }
        try {
            if (class_exists($controller) === false) {
                throw new Exception("Controller '$controller' doesn't exist.");
            }

            $controller = new $controller;
            $action = $params[1];

            if (method_exists($controller, $action) === false) {
                throw new Exception("Action '$action' doesn't exist.");
            }
            return call_user_func_array(
                [$controller, $action],
                $vars
            );
        } catch (Exception $exception) {
            echo 'HTTP Error 404 Not Found';
        }
        break;
}
