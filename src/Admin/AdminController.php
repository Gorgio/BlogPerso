<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 02/03/2016
 * Time: 13:56
 */

namespace App\Admin;

use App\Category\CategoryManager;
use App\DBLayer\DatabaseLayer;
use App\Rank\RankManager;
use App\Renderer\TwigRenderer;
use App\Sessions\Sessions;
use Core\Generate\Generate;
use Core\Package\AbstractController;

/**
 * Class AdminController
 *
 * @package App\Controller
 */
class AdminController extends AbstractController
{
    public function dashboard()
    {
        $csrfToken = Generate::csrfToken();
        Sessions::set(Sessions::CSRF_TOKEN_KEY, $csrfToken);

        $vars = ['csrfToken' => $csrfToken];
        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setView('dashboard');

        try {
            $databaseLayer = new DatabaseLayer();
            $databaseLayer->useRead();
            $dbInstance = $databaseLayer->getDbHandler();
            $categoryManager = new CategoryManager();
            $categoriesFetched = $categoryManager->getList($dbInstance);
            if ($categoriesFetched === false) {
                throw new \UnexpectedValueException();
            }
            $vars['categories'] = $categoryManager->getResults();
            $rankManager = new RankManager();
            $ranksFetched = $rankManager->getList($dbInstance);
            if ($ranksFetched === false) {
                throw new \UnexpectedValueException();
            }
            $vars['ranks'] = $rankManager->getResults();
        } catch (\UnexpectedValueException $e) {
            $renderer->setView('@errors/internal');
        }

        $renderer->setVars($vars);
        $renderer->render();
    }
}
