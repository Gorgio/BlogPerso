<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 02/04/2016
 * Time: 19:25
 */

namespace App\Category;

use App\DBLayer\DatabaseLayer;
use App\Sessions\Sessions;
use App\Traits\FormatChecks;
use Core\Globals\Post;
use Core\Package\AbstractController;
use Core\Redirection\Redirection;

/**
 * Class CategoriesController
 *
 * @category Class
 * @package  App\Category
 */
class CategoryController extends AbstractController
{
    use FormatChecks;

    /**
     *  Add category to DB
     * @param array $post
     */
    public function add(array $post)
    {
        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            $category = new Category($post);
            $dbConnectionHandler = new DatabaseLayer();
            $dbConnectionHandler->useCRU();
            $dbInstance = $dbConnectionHandler->getDbHandler();
            $categoryManager = new CategoryManager();
            $categoryManager->add($dbInstance, $category);
            $this->setMessages($categoryManager->getMessages());
        }

        Sessions::addAlertList($this->getMessages());
        Sessions::del(Sessions::CSRF_TOKEN_KEY);
        $redirect = new Redirection();
        $redirect::redirect('admin');
    }

    /**
     * Edit category in DB
     * @param array $post
     */
    public function edit(array $post)
    {
        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            $category = new Category($post);
            $dbConnectionHandler = new DatabaseLayer();
            $dbConnectionHandler->useCRU();
            $dbInstance = $dbConnectionHandler->getDbHandler();
            $categoryManager = new CategoryManager();
            $categoryManager->set($dbInstance, $category);
            $this->setMessages($categoryManager->getMessages());
        }

        Sessions::addAlertList($this->getMessages());
        Sessions::del(Sessions::CSRF_TOKEN_KEY);
        $redirect = new Redirection();
        $redirect::redirect('admin');
    }

    /**
     * Delete category in DB
     * @param array $post
     */
    public function delete(array $post)
    {
        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            $category = new Category($post);
            $dbConnectionHandler = new DatabaseLayer();
            $dbConnectionHandler->useCRUD();
            $dbInstance = $dbConnectionHandler->getDbHandler();
            $categoryManager = new CategoryManager();
            $categoryManager->del($dbInstance, $category);
            $this->setMessages($categoryManager->getMessages());
        }

        Sessions::addAlertList($this->getMessages());
        Sessions::del(Sessions::CSRF_TOKEN_KEY);
        $redirect = new Redirection();
        $redirect::redirect('admin');
    }
}
