<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 01/04/2016
 * Time: 18:22
 */

namespace App\Category;

use App\Traits\FormatChecks;
use Core\Package\AbstractObject;

/**
 * Class Category
 *
 * @category Class
 * @package  App\Objects
 */
class Category extends AbstractObject
{
    use FormatChecks;

    protected $id = null;
    protected $name = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $idCategory
     * @return bool
     */
    public function setId($idCategory)
    {
        if ($this->isPositiveInteger($idCategory) === true) {
            $this->id = $idCategory;
            return true;
        }
        return false;
    }

    /**
     * @param mixed $name
     * @return bool
     */
    public function setName($name)
    {
        if ($this->isName($name) === true) {
            $this->name = $name;
            return true;
        }
        return false;
    }
}
