<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 16/12/2016
 * Time: 21:16
 */

namespace App\Category;

use Core\Message\MessageType;
use Core\Package\AbstractManager;
use Core\Package\ManagerMessages;
use PDO;

/**
 * Class CategoriesManagerNew
 * @package App\Category
 */
class CategoryManager extends AbstractManager
{
    /**
     * @param PDO $dbInstance
     * @param Category $category
     * @return bool
     */
    public function add(PDO $dbInstance, Category $category)
    {
        $validData = $category->checkData(['name']);
        if ($validData === false) {
            $this->setMessages($category->getMessages());
            return false;
        }
        $isDataAvailable = $this->checkIsAvailable($dbInstance, ['name'], $category);

        if ($isDataAvailable === false) {
            return false;
        }
        $this->addField('name', $category->getName(), PDO::PARAM_STR);
        $status = $this->insert($dbInstance);
        if ($status === false) {
            $this->setMessage(ManagerMessages::ADD_FAILURE, MessageType::APP_ERROR);
            return false;
        }
        $this->setMessage(
            ManagerMessages::ADD_SUCCESS,
            MessageType::SUCCESS,
            ['object' => $category->getClassShortName()]
        );
        return true;
    }

    /**
     * @param PDO $dbInstance
     * @return array
     */
    public function getList(PDO $dbInstance)
    {
        $this->order('name', 'ASC');
        $object = new Category();
        return $this->getListOf($dbInstance, $object);
    }

    /**
     * @param PDO $dbInstance
     * @param Category $category
     * @return bool
     */
    public function set(PDO $dbInstance, Category $category)
    {
        $validData = $category->checkData(['id', 'name']);
        if ($validData === false) {
            $this->setMessages($category->getMessages());
            return false;
        }
        $isCategoryExistant = $this->getById($dbInstance, $category->getId(), PDO::PARAM_INT);

        if ($isCategoryExistant === false) {
            $this->setMessage(
                ManagerMessages::SET_INEXISTANT_ELEMENT,
                MessageType::ERROR,
                ['object' => $category->getClassShortName()]
            );
            return false;
        }

        $categoryDataInDb = $this->getResults();
        $categoryAsInDb = new Category($categoryDataInDb);

        $isDataAvailable = $this->checkDifferenceIsAvailable($dbInstance, ['name'], $category, $categoryAsInDb);

        if ($isDataAvailable === false) {
            return false;
        }

        $this->addField('name', $category->getName(), PDO::PARAM_STR);
        $this->where('id', $category->getId(), PDO::PARAM_INT);
        $status = $this->update($dbInstance);

        if ($status === false) {
            $this->setMessage(ManagerMessages::SET_FAILURE, MessageType::APP_ERROR);
            return false;
        }
        $this->setMessage(
            ManagerMessages::SET_SUCCESS,
            MessageType::SUCCESS,
            ['object' => $category->getClassShortName()]
        );
        return true;
    }

    /**
     * @param PDO $dbInstance
     * @param Category $category
     * @return bool
     */
    public function del(PDO $dbInstance, Category $category)
    {
        $validData = $category->checkData(['id']);
        if ($validData === false) {
            $this->setMessages($category->getMessages());
            return false;
        }

        $this->where('id', $category->getId(), PDO::PARAM_INT);
        $status = $this->deleteFrom($dbInstance);

        if ($status === false) {
            $this->setMessage(ManagerMessages::DEL_FAILURE, MessageType::APP_ERROR);
            return false;
        }

        $this->setMessage(
            ManagerMessages::DEL_SUCCESS,
            MessageType::SUCCESS,
            ['object' => $category->getClassShortName()]
        );
        return true;
    }
}
