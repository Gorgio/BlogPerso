<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/02/2016
 * Time: 19:07
 */

namespace App\Traits;

use Core\Traits\CoreFormatChecks;

/**
 * Class FormatChecks
 * @package App\Traits
 */
trait FormatChecks
{
    use CoreFormatChecks;

    /**
     * Letters/Numbers/space/dot/dash between 4 and $maxLength
     *
     * @param string $name      Name to test
     * @param int    $maxLength (default 40) inclusive
     *
     * @return bool
     */
    protected function isName(string $name, $maxLength = 40)
    {
        // REGEX : \p{L} means All unicode letters, \p{Nd} means decimal number.
        if (preg_match('#^[\p{L}\p{Nd} .-]{4,'. $maxLength .'}$#', $name) === 1) {
            return true;
        }
        return false;
    }

    /**
     * @param $string
     * @param int $length
     * @return bool
     */
    protected function isAlphaNumString($string, int $length)
    {
        if (preg_match('#^([\w-]){' . $length . '}$#', $string) === 1) {
            return true;
        }
        return false;
    }
}
