<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 06/11/2016
 * Time: 14:18
 */

namespace App\User;

use App\Rank\Rank;
use App\Traits\FormatChecks;
use Core\Package\AbstractObject;

/**
 * Class User
 * @package App\User
 */
class User extends AbstractObject
{
    use FormatChecks;

    const ID_LENGTH = 8;
    const NAME_MAX_LENGTH = 80;

    protected $id                 = null;
    protected $login              = null;
    protected $mail               = null;
    protected $name               = null;
    protected $dateRegister       = null;
    protected $password           = null;
    protected $rank               = null;
    protected $rankId             = null;
    protected $rankName           = null;
    protected $rankPrivilegeLevel = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $idUser
     * @return bool
     */
    public function setId($idUser)
    {
        if ($this->isAlphaNumString($idUser, self::ID_LENGTH) === true) {
            $this->id = $idUser;
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     * @return bool
     */
    public function setLogin($login)
    {
        if ($this->isName($login, self::NAME_MAX_LENGTH) === true) {
            $this->login = $login;
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     * @return bool
     */
    public function setMail($mail)
    {
        if ($this->isMail($mail) === true) {
            $this->mail = $mail;
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getDateRegister()
    {
        return $this->dateRegister;
    }

    /**
     * @param mixed $dateRegister
     * @return bool
     */
    public function setDateRegister($dateRegister)
    {
        $this->dateRegister = $dateRegister;
        return true;
    }

    /**
     * @return mixed
     */
    public function getRankId()
    {
        return $this->rankId;
    }
    /**
     * @param mixed $rankId
     * @return bool
     */
    public function setRankId($rankId)
    {
        $rank = new Rank();
        $rankValid = $rank->setId($rankId);
        if ($rankValid === true) {
            $this->rankId = $rankId;
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getRankPrivilegeLevel()
    {
        return $this->rankPrivilegeLevel;
    }

    /**
     * @param mixed $rankPrivilegeLevel
     * @return bool
     */
    public function setRankPrivilegeLevel($rankPrivilegeLevel)
    {
        $rank = new Rank();
        $valid = $rank->setPrivilegeLevel($rankPrivilegeLevel);
        if ($valid === true) {
            $this->rankPrivilegeLevel = $rankPrivilegeLevel;
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getRankName()
    {
        return $this->rankName;
    }

    /**
     * @param mixed $rankName
     * @return bool
     */
    public function setRankName($rankName)
    {
        $rank = new Rank();
        $valid = $rank->setName($rankName);
        if ($valid === true) {
            $this->rankName = $rankName;
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return bool
     */
    public function setName($name)
    {
        if ($this->isName($name, User::NAME_MAX_LENGTH) === true) {
            $this->name = $name;
            return true;
        }
        return false;
    }

    /**
     * @return null|Rank
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param Rank $rank
     * @return bool
     */
    public function setRank($rank)
    {
        if ($rank instanceof Rank) {
            $this->rank = $rank;
            return true;
        }
        return false;
    }

    /**
     * @return null|string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param null|string $password
     */
    public function setPassword(string $password = null)
    {
        $this->password = $password;
    }
}
