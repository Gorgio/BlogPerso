<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 06/11/2016
 * Time: 14:16
 */

namespace App\User;

use App\DBLayer\DatabaseLayer;
use App\Rank\Rank;
use App\Rank\RankManager;
use App\Renderer\TwigRenderer;
use App\Sessions\Sessions;
use App\Traits\FormatChecks;
use Core\Generate\Generate;
use Core\Globals\Post;
use Core\Message\MessageType;
use Core\Package\AbstractController;
use Core\Redirection\Redirection;
use Exception;

/**
 * Class UsersController
 *
 * @package App\Controller
 */
class UserController extends AbstractController
{
    use FormatChecks;

    /**
     * @param int $page
     */
    public function listUsersForAdmin($page = 1)
    {
        $databaseLayer = new DatabaseLayer();
        $databaseLayer->useRead();
        $dbInstance = $databaseLayer->getDbHandler();

        $userManager = new UserManager();
        $userManager->nbRows($dbInstance);
        $nbRows = $userManager->getNbRows();
        $page = intval($page);
        $elementsPerPage = 5;
        // Calculate totalPages.
        $totalPages = intval(ceil($nbRows / $elementsPerPage));

        $userManager->getList($dbInstance, $page, $elementsPerPage);
        $users = $userManager->getResults();
        $vars = [
            'users' => $users,
            'page' => $page,
            'totalPages' => $totalPages
        ];

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setView('adminDashboard');
        $renderer->setVars($vars);
        $renderer->render();
    }

    /**
     *  Display connection form
     */
    public function connectForm()
    {
        /*if (Sessions::isUserConnected() === true) {
            Sessions::addAlertList($this->getMessages());
            Sessions::del(Sessions::CSRF_TOKEN_KEY);
            $redirect = new Redirection();
            $redirect::redirect('');
        }*/

        $csrfToken = Generate::csrfToken();
        Sessions::set(Sessions::CSRF_TOKEN_KEY, $csrfToken);

        $vars = ['csrfToken' => $csrfToken];

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setView('connectForm');
        $renderer->setVars($vars);
        $renderer->render();
    }

    /**
     *  Connect user
     * @param array $post
     */
    public function connect(array $post)
    {
        $redirectTo = '';
        try {
            if (Sessions::isUserConnected() === true) {
                throw new Exception('You are already connected.');
            }
            $csrfPassed = $this->isCsrfProtectionPassed(
                Post::getKey(Post::CSRF_TOKEN_KEY),
                Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
            );
            $redirectTo = 'connect';

            if ($csrfPassed === false) {
                throw new Exception('Internal Error.');
            }
            if (key_exists('login', $post) === false) {
                $this->setMessage('Login hasn\'t been provided.', MessageType::ERROR);
            }
            if (key_exists('password', $post) === false) {
                $this->setMessage('Password hasn\'t been provided.', MessageType::ERROR);
            }

            if (empty($this->getMessages()) === false) {
                throw new Exception(null);
            }

            $databaseLayer = new DatabaseLayer();
            $databaseLayer->useRead();
            $dbInstance = $databaseLayer->getDbHandler();

            $userManager = new UserManager();
            $status = $userManager->connectUser(
                $dbInstance,
                $post['login'],
                $post['password']
            );

            if ($status === false) {
                throw new Exception('Wrong login-password combination.');
            }

            $userData = $userManager->getResults();
            if (($userData instanceof User) === false) {
                throw new Exception('Wrong login-password combination.');
            }
            Sessions::connectUser(
                $userData->getLogin(),
                $userData->getRankName()
            );
            $this->setMessage('You are now connected.', MessageType::SUCCESS);
            $redirectTo = '';
        } catch (Exception $exception) {
            if (is_null($exception->getMessage()) === false) {
                $this->setMessage($exception->getMessage(), MessageType::ERROR);
            }
        }

        Sessions::addAlertList($this->getMessages());
        Sessions::del(Sessions::CSRF_TOKEN_KEY);
        $redirect = new Redirection();
        $redirect::redirect($redirectTo);
    }

    /**
     *  Logout user
     */
    public function disconnect()
    {
        Sessions::disconnectUser();
        Sessions::addAlert('You have been successfully disconnected.', MessageType::SUCCESS);
        $redirect = new Redirection();
        $redirect::redirect('');
    }

    /**
     * Admin Form to add a User to the DB
     */
    public function addUser()
    {
        $csrfToken = Generate::csrfToken();
        Sessions::set(Sessions::CSRF_TOKEN_KEY, $csrfToken);

        $databaseLayer = new DatabaseLayer();
        $databaseLayer->useRead();
        $dbInstance = $databaseLayer->getDbHandler();
        $rankManager = new RankManager();
        $rankManager->getList($dbInstance);
        $ranks = $rankManager->getResults();

        $vars = ['ranks' => $ranks, 'csrfToken' => $csrfToken];

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setVars($vars);
        $renderer->setView('addUser');
        $renderer->render();
    }

    /**
     * Admin method to insert user into DB
     * @param array $post
     */
    public function addUserToDB(array $post)
    {
        $redirectTo = 'admin/users/add/';

        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            if (is_null(Post::getKey('password') === true)) {
                $this->setMessage('Password ins\'t defined.', MessageType::ERROR);
            }
            if (is_null(Post::getKey('confirm_password') === true)) {
                $this->setMessage('Please retype your password.', MessageType::ERROR);
            }
            if (Post::getKey('password') !== Post::getKey('confirm_password')) {
                $this->setMessage('Your passwords don\'t match.', MessageType::ERROR);
            }

            if (empty($this->getMessages()) === true) {
                $user = new User($post);

                $databaseLayer = new DatabaseLayer();
                $databaseLayer->useRead();
                $dbInstance = $databaseLayer->getDbHandler();

                if (is_null(Post::getKey('rank')) === false) {
                    $rank = new Rank();
                    $rank->setId(Post::getKey('rank'));
                    $validRank = $rank->checkData(['id']);
                    if ($validRank === true) {
                        $rankManager = new RankManager();
                        $isRankExistant = $rankManager->isExistant($dbInstance, $rank->getId());
                        if ($isRankExistant === true) {
                            $user->setRank($rank);
                        } else {
                            $this->setMessage('Rank selected doesn\'t exist.', MessageType::APP_ERROR);
                        }
                        unset($rankManager);
                    } else {
                        $this->setMessage('Rank provided is invalid.', MessageType::APP_ERROR);
                    }
                } else {
                    $this->setMessage('No rank provided.', MessageType::APP_ERROR);
                }

                if (empty($this->getMessages()) === true) {
                    $userManager = new UserManager();
                    $databaseLayer->useCRU();
                    $dbInstance = $databaseLayer->getDbHandler();

                    $status = $userManager->addUser($dbInstance, $user);

                    if ($status === true) {
                        $redirectTo = 'admin/users/';
                    }
                    $this->setMessages($userManager->getMessages());
                }
            }
        }

        Sessions::addAlertList($this->getMessages());
        Sessions::del(Sessions::CSRF_TOKEN_KEY);
        $redirect = new Redirection();
        $redirect::redirect($redirectTo);
    }

    /**
     * Show edit user form / view
     *
     * @param $idUser
     */
    public function editUser($idUser)
    {
        $csrfToken = Generate::csrfToken();
        Sessions::set(Sessions::CSRF_TOKEN_KEY, $csrfToken);

        $databaseLayer = new DatabaseLayer();
        $databaseLayer->useRead();
        $dbInstance = $databaseLayer->getDbHandler();
        $rankManager = new RankManager();
        $rankManager->getList($dbInstance);
        $ranks = $rankManager->getResults();

        $dbInstance = $databaseLayer->getDbHandler();
        $userManager = new UserManager();
        $status = $userManager->getById($dbInstance, $idUser, \PDO::PARAM_STR);

        if ($status === false) {
            Sessions::addAlert('Cet utilisateur n\'existe pas.', MessageType::ERROR);
            $redirect = new Redirection();
            $redirect::redirect('admin/users/');
        }

        $userData = $userManager->getResults();
        $user = new User($userData);

        $vars = ["user" => $user, "ranks" => $ranks, 'csrfToken' => $csrfToken];

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setVars($vars);
        $renderer->setView('editUserAdmin');
        $renderer->render();
    }

    /**
     * Admin edit method
     * @param array $post
     */
    public function editUserToDB(array $post)
    {
        $redirectTo = 'admin/users/';

        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            $user = new User($post);

            $databaseLayer = new DatabaseLayer();
            $databaseLayer->useRead();
            $dbInstance = $databaseLayer->getDbHandler();

            if (is_null(Post::getKey('rank')) === false) {
                $rank = new Rank();
                $rank->setId(Post::getKey('rank'));
                $validRank = $rank->checkData(['id']);
                if ($validRank === true) {
                    $rankManager = new RankManager();
                    $isRankExistant = $rankManager->isExistant($dbInstance, $rank->getId());
                    if ($isRankExistant === true) {
                        $user->setRank($rank);
                    } else {
                        $this->setMessage('Rank selected doesn\'t exist.', MessageType::APP_ERROR);
                    }
                    unset($rankManager);
                } else {
                    $this->setMessage('Rank provided is invalid.', MessageType::APP_ERROR);
                }
            } else {
                $this->setMessage('No rank provided.', MessageType::APP_ERROR);
            }

            if (empty($this->getMessages()) === true) {
                $userManager = new UserManager();
                $databaseLayer->useCRU();
                $dbInstance = $databaseLayer->getDbHandler();
                $status = $userManager->editUser($dbInstance, $user);
                $redirectTo = 'admin/users/';
                if ($status === false) {
                    $redirectTo = 'admin/users/edit/' . $user->getId() . DS;
                }
                $this->setMessages($userManager->getMessages());
            }
        }

        Sessions::addAlertList($this->getMessages());
        Sessions::del(Sessions::CSRF_TOKEN_KEY);
        $redirect = new Redirection();
        $redirect::redirect($redirectTo);
    }
}
