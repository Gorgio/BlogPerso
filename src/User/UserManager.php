<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 06/11/2016
 * Time: 14:18
 */

namespace App\User;

use App\Rank\Rank;
use Core\Database\JoinClauseTypes;
use Core\Database\ValueGeneratedFunctions;
use Core\Generate\Generate;
use Core\Message\MessageType;
use Core\Package\AbstractManager;
use Core\Package\ManagerMessages;
use PDO;

/**
 * Class UsersManager
 * @package App\Managers
 */
class UserManager extends AbstractManager
{
    /**
     * @param $dbInstance
     * @param int $page
     * @param int $elementsPerPage
     * @return mixed
     */
    public function getList($dbInstance, int $page, int $elementsPerPage)
    {
        $this->addColumn('*');
        $this->addColumn('user.id', null, 'id');
        $this->addColumn('user.name', null, 'user_name');
        $this->addColumn('rank.id', null, 'rank_id');
        $this->addColumn('rank.name', null, 'rank_name');
        $this->join(JoinClauseTypes::LEFT, 'rank', 'rank_id', 'id');
        $this->order('date_register', 'DESC');
        $this->getListFromPage($dbInstance, new User(), $page, $elementsPerPage);
        return $this->getResults();
    }

    /**
     * Add a new user to the DataBase
     *
     * @param PDO $dbInstance
     * @param User $user
     *
     * @return bool
     */
    public function addUser(PDO $dbInstance, User $user)
    {
        $isUserValid = $user->checkData(['login', 'mail', 'name', 'password', 'rank']);
        if ($isUserValid === false) {
            $this->setMessages($user->getMessages());
            return false;
        }

        $isDataAvailable = $this->checkIsAvailable($dbInstance, ['login', 'mail', 'name'], $user);
        if ($isDataAvailable === false) {
            return false;
        }

        $idUser = Generate::randomAlphaNumString(User::ID_LENGTH);
        $isIdAvailable = $this->isAvailable($dbInstance, 'id', $idUser, PDO::PARAM_STR);
        if ($isIdAvailable === false) {
            return $this->addUser($dbInstance, $user);
        }

        $hashedPassword = $this->hashPwd($user->getPassword());

        $this->addField('id', $idUser, PDO::PARAM_STR);
        $this->addField('login', $user->getLogin(), PDO::PARAM_STR);
        $this->addField('mail', $user->getMail(), PDO::PARAM_STR);
        $this->addField('name', $user->getName(), PDO::PARAM_STR);
        $this->addField('password', $hashedPassword, PDO::PARAM_STR);
        $this->addField('date_register', ValueGeneratedFunctions::NOW);
        $this->addField('rank_id', $user->getRank()->getId(), PDO::PARAM_INT);

        $status = $this->insert($dbInstance);
        if ($status === false) {
            $this->setMessage(ManagerMessages::ADD_FAILURE, MessageType::APP_ERROR);
            return false;
        }

        $this->setMessage(
            ManagerMessages::ADD_SUCCESS,
            MessageType::SUCCESS,
            ['object' => $user->getClassShortName()]
        );
        return true;
    }

    /**
     * Edit a user which already is in the DataBase.
     *
     * @param PDO $dbInstance
     * @param User $user
     * @return bool
     */
    public function editUser(PDO $dbInstance, User $user)
    {
        $isUserValid = $user->checkData(['id', 'login', 'mail', 'name', 'rank']);
        if ($isUserValid === false) {
            $this->setMessages($user->getMessages());
            return false;
        }

        $isUserExistant = $this->getById($dbInstance, $user->getId(), 'string');
        if ($isUserExistant === false) {
            $this->setMessage(
                ManagerMessages::SET_INEXISTANT_ELEMENT,
                MessageType::ERROR,
                ['object' => $user->getClassShortName()]
            );
            return false;
        }

        $userDataInDB = $this->getResults();
        $userAsInDB = new User($userDataInDB);

        $isDataAvailable = $this->checkDifferenceIsAvailable(
            $dbInstance,
            ['login', 'mail', 'name'],
            $user,
            $userAsInDB
        );

        if ($isDataAvailable === false) {
            return false;
        }

        $this->addField('login', $user->getLogin(), PDO::PARAM_STR);
        $this->addField('mail', $user->getMail(), PDO::PARAM_STR);
        $this->addField('rank_id', $user->getRank()->getId(), PDO::PARAM_INT);
        $this->addField('name', $user->getName(), PDO::PARAM_STR);
        $this->where('id', $user->getId(), PDO::PARAM_STR);

        $status = $this->update($dbInstance);
        if ($status === false) {
            $this->setMessage(ManagerMessages::SET_FAILURE, MessageType::APP_ERROR);
            return false;
        }

        $this->setMessage(
            ManagerMessages::SET_SUCCESS,
            MessageType::SUCCESS,
            ['object' => $user->getClassShortName()]
        );
        return true;
    }

    /**
     * @param PDO $dbInstance
     * @param $rank
     * @param $fallbackRank
     * @return bool
     */
    public function editUserGroupRank(PDO $dbInstance, Rank $rank, Rank $fallbackRank)
    {
        $propertiesToCheck = ['id'];
        $isRankValid = $rank->checkData($propertiesToCheck);
        $isFallbackRankValid = $fallbackRank->checkData($propertiesToCheck);
        if ($isRankValid !== true or $isFallbackRankValid !== true) {
            $this->setMessages($rank->getMessages());
            $this->setMessages($fallbackRank->getMessages());
            return false;
        }

        $this->addField('rank_id', $fallbackRank->getId(), PDO::PARAM_INT, 'new_rank');
        $this->where('rank_id', $rank->getId(), PDO::PARAM_INT);

        return $this->update($dbInstance);
    }

    /**
     * Connect user
     * If success user details are available with getResult()
     *
     * @param PDO $dbInstance
     * @param $login
     * @param $password
     * @return bool
     */
    public function connectUser(PDO $dbInstance, $login, $password)
    {
        $this->join(JoinClauseTypes::INNER, 'rank', 'rank_id', 'id');
        $this->where('login', $login, PDO::PARAM_STR)->select($dbInstance);
        $this->singleOutResults();
        $data = $this->getResults();
        if (is_null($data) !== false) {
            $this->setResults(null);
            return false;
        }

        $user = new User($data);

        $isPwdCorrect = $this->verifyPwd($password, $user->getPassword());
        if ($isPwdCorrect !== true) {
            $this->setResults(null);
            return false;
        }
        // Set password to null to prevent hash leaking.
        $user->setPassword(null);

        $this->setResults($user);
        return true;
    }

    /**
     * @param $password
     * @return bool|string
     */
    private function hashPwd($password)
    {
        $hash = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);
        return $hash;
    }

    /**
     * @param $userPwd
     * @param $hashedPwd
     * @return bool
     */
    private function verifyPwd($userPwd, $hashedPwd)
    {
        return password_verify($userPwd, $hashedPwd);
    }
}
