<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/03/2016
 * Time: 14:14
 */

namespace App\Sessions;

use Core\Message\Message;
use Core\Message\MessageType;
use Core\Globals\Session as BaseSessions;
use Core\System\System;

/**
 * Class Sessions
 * @package App\Objects
 */
class Sessions extends BaseSessions
{
    use Message;

    const USER_SESSION_KEY = 'user';
    const INTERNAL_ERROR = 'An internal error has occurred.';

    const ALERT_KEY = 'alerts';

    /*
     * Alerts
     *  Alerts Structure
     *      SESSION['Alerts']
     *          List Array that contains all the messages
     *          Array that contains the Alerts MetaData (Message & Type)
     *
     *  Alerts types: Success || Error || Info || Warning
     */

    /**
     * @param $message
     * @param string $type
     */
    public static function addAlert($message, $type = MessageType::ERROR)
    {
        if (empty($message) === true or is_string($message) === false) {
            $message = 'No message has been set.';
        }

        switch (strtolower($type)) {
            case MessageType::APP_ERROR:
                $type = MessageType::ERROR;

                if (System::isDevelopmentEnvironment() === false) {
                    $message = self::INTERNAL_ERROR;
                }
                break;

            case MessageType::ERROR:
                $type = MessageType::ERROR;
                break;

            case MessageType::INFO:
                $type = MessageType::INFO;
                break;

            case MessageType::SUCCESS:
                $type = MessageType::SUCCESS;
                break;

            case MessageType::WARNING:
                $type = MessageType::WARNING;
                break;


            default:
                $type = MessageType::INFO;
                $message .= "\n No correct type has been set.";
        }

        $alerts = static::getKey(self::ALERT_KEY);
        $alerts[] = [
            'message' => $message,
            'type' => $type
        ];
        static::set(self::ALERT_KEY, $alerts);
    }

    /**
     * Add multiple alerts in one go
     * @param array $messageList
     * @throws InvalidAlertDataException
     */
    public static function addAlertList(array $messageList)
    {
        foreach ($messageList as $data) {
            if (is_array($data) === false) {
                throw new InvalidAlertDataException("Alert is not in array format.");
            }
            if (key_exists('message', $data) === false) {
                throw new InvalidAlertDataException('No message key found.');
            }
            if (key_exists('type', $data) === false) {
                throw new InvalidAlertDataException('No type key found.');
            }

            static::addAlert($data['message'], $data['type']);
        }
    }

    /**
     * Generate HTML Display for alerts
     * @return string
     */
    public static function getHTMLAlertDisplay()
    {
        if (static::sessionExist(self::ALERT_KEY) === true) {
            $display = '';
            foreach (static::getKey(self::ALERT_KEY) as $data) {
                $display .= '<div class="l-alert alert alert-' . $data['type'] . '">
                    <div class="l-vertical-center l-alert-icon alert-icon">
                        <i class="fa fa-2x fa-' . $data['type'] . ' icon"></i>
                    </div>
                    <p class="l-vertical-center l-alert-content">' . nl2br($data['message']) . '</p>
                    <div class="l-vertical-center l-alert-close alert-close">
                        <i class="fa fa-close"></i>
                    </div>
                </div>';
            }

            return $display;
        }

        return '';
    }

    /**
     * Display generated HTML for alerts
     */
    public static function displayAlerts()
    {
        if (static::sessionExist(self::ALERT_KEY) === true) {
            echo self::getHTMLAlertDisplay();

            static::del(self::ALERT_KEY);
        }
    }

    // User session.
    /**
     *  Affects logged user data in designed key while
     * reset-ing Session Id to prevent session fixation
     *
     * @param $name
     * @param $rankName
     */
    public static function connectUser($name, $rankName)
    {
        $data = ['name' => $name, 'rank_name' => $rankName];
        static::set(self::USER_SESSION_KEY, $data);
        static::regenerateId();
    }

    /**
     *  LogOut User safely
     */
    public static function disconnectUser()
    {
        static::destroy();
        static::init();
    }

    /**
     * @return bool
     */
    public static function isUserConnected()
    {
        return static::sessionExist(self::USER_SESSION_KEY);
    }
}
