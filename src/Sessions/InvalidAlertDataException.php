<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 14/02/2017
 * Time: 22:54
 */

namespace App\Sessions;

/**
 * Class InvalidAlertDataException
 * @package App\Sessions
 */
class InvalidAlertDataException extends \Exception
{

}
