<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 17/12/2016
 * Time: 12:45
 */

namespace App\Rank;

use App\User\UserManager;
use Core\Message\MessageType;
use Core\Package\AbstractManager;
use Core\Package\ManagerMessages;
use PDO;

/**
 * Class RanksManager
 * @package App\Rank
 */
class RankManager extends AbstractManager
{
    /**
     * @param PDO $dbInstance
     * @param Rank $rank
     * @return bool
     */
    public function add(PDO $dbInstance, Rank $rank)
    {
        $validData = $rank->checkData(['name', 'privilegeLevel']);
        if ($validData === false) {
            $this->setMessages($rank->getMessages());
            return false;
        }

        $isDataAvailable = $this->checkIsAvailable($dbInstance, ['name'], $rank);
        if ($isDataAvailable === false) {
            return false;
        }

        $this->addField('name', $rank->getName(), PDO::PARAM_STR);
        $this->addField('privilege_level', $rank->getPrivilegeLevel(), PDO::PARAM_INT);
        $status = $this->insert($dbInstance);

        if ($status === false) {
            $this->setMessage(ManagerMessages::ADD_FAILURE, MessageType::APP_ERROR);
            return false;
        }
        $this->setMessage(
            ManagerMessages::ADD_SUCCESS,
            MessageType::SUCCESS,
            ['object' => $rank->getClassShortName()]
        );
        return true;
    }

    /**
     * @param PDO $dbInstance
     * @return array
     */
    public function getList(PDO $dbInstance)
    {
        $this->order('privilege_level', 'DESC');
        $object = new Rank();
        return $this->getListOf($dbInstance, $object);
    }

    /**
     * @param PDO $dbInstance
     * @param Rank $rank
     * @return bool
     */
    public function set(PDO $dbInstance, Rank $rank)
    {
        $validData = $rank->checkData(['id', 'name', 'privilegeLevel']);
        if ($validData === false) {
            $this->setMessages($rank->getMessages());
            return false;
        }
        $isRankExistant = $this->getById($dbInstance, $rank->getId(), 'int');
        if ($isRankExistant === false) {
            $this->setMessage(
                ManagerMessages::SET_INEXISTANT_ELEMENT,
                MessageType::ERROR,
                ['object' => $rank->getClassShortName()]
            );
            return false;
        }
        $rankDataInDb = $this->getResults();
        $rankAsInDb = new Rank($rankDataInDb);

        $isDataAvailable = $this->checkDifferenceIsAvailable($dbInstance, ['name'], $rank, $rankAsInDb);
        if ($isDataAvailable === false) {
            return false;
        }

        $this->addField('name', $rank->getName(), PDO::PARAM_STR);
        $this->addField('privilege_level', $rank->getPrivilegeLevel(), PDO::PARAM_INT);
        $this->where('id', $rank->getId(), PDO::PARAM_INT);
        $status = $this->update($dbInstance);

        if ($status === false) {
            $this->setMessage(ManagerMessages::SET_FAILURE, MessageType::APP_ERROR);
            return false;
        }

        $this->setMessage(
            ManagerMessages::SET_SUCCESS,
            MessageType::SUCCESS,
            ['object' => $rank->getClassShortName()]
        );
        return true;
    }

    /**
     * @param PDO $dbInstance
     * @param Rank $rank
     * @param $fallbackRank
     * @param UserManager $userManager
     * @return bool|true
     */
    public function del(PDO $dbInstance, Rank $rank, Rank $fallbackRank, UserManager $userManager)
    {
        $propertiesToCheck = ['id'];
        $isRankValid = $rank->checkData($propertiesToCheck);
        $isFallbackRankValid = $fallbackRank->checkData($propertiesToCheck);
        if ($isFallbackRankValid === false || $isRankValid === false) {
            $this->setMessages($rank->getMessages());
            $this->setMessages($fallbackRank->getMessages());
            return false;
        }
        if ($rank->getId() === $fallbackRank->getId()) {
            $this->setMessage('Fallback rank provided is identical to rank to be removed.', MessageType::ERROR);
            return false;
        }

        $userChangeApplied = $userManager->editUserGroupRank($dbInstance, $rank, $fallbackRank);
        if ($userChangeApplied === false) {
            $this->setMessages($userManager->getMessages());
            return false;
        }

        $status = $this->delById($dbInstance, $rank->getId());
        if ($status === false) {
            $this->setMessage(ManagerMessages::DEL_FAILURE, MessageType::APP_ERROR);
            return false;
        }

        $this->setMessage(
            ManagerMessages::DEL_SUCCESS,
            MessageType::SUCCESS,
            ['object' => $rank->getClassShortName()]
        );
        return true;
    }

    /**
     * @param $dbInstance
     * @param $idRank
     * @return bool
     */
    public function isExistant(PDO $dbInstance, $idRank)
    {
        $this->where('id', $idRank, PDO::PARAM_INT);
        $this->nbRows($dbInstance);

        if ($this->getNbRows() === 0) {
            return false;
        }
        return true;
    }
}
