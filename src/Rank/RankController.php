<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 02/09/2016
 * Time: 14:15
 */

namespace App\Rank;

use App\DBLayer\DatabaseLayer;
use App\Sessions\Sessions;
use App\Traits\FormatChecks;
use App\User\UserManager;
use Core\Globals\Post;
use Core\Message\MessageType;
use Core\Package\AbstractController;
use Core\Redirection\Redirection;

/**
 * Class RanksController
 * @package App\Controller
 */
class RankController extends AbstractController
{
    use FormatChecks;

    /**
     * Method which adds a rank in the DB
     * @param array $post
     */
    public function add(array $post)
    {
        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            $rank = new Rank($post);

            $dbConnectionHandler = new DatabaseLayer();
            $dbConnectionHandler->useCRU();
            $dbInstance = $dbConnectionHandler->getDbHandler();
            $rankManager = new RankManager();
            $rankManager->add($dbInstance, $rank);
            $this->setMessages($rankManager->getMessages());
        }

        Sessions::addAlertList($this->getMessages());
        Sessions::del(Sessions::CSRF_TOKEN_KEY);
        $redirect = new Redirection();
        $redirect::redirect('admin');
    }

    /**
     * Method which updates a rank in the DB
     * @param array $post
     */
    public function edit(array $post)
    {
        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            $rank = new Rank($post);

            $dbConnectionHandler = new DatabaseLayer();
            $dbConnectionHandler->useCRU();
            $dbInstance = $dbConnectionHandler->getDbHandler();
            $rankManager = new RankManager();
            $rankManager->set($dbInstance, $rank);
            $this->setMessages($rankManager->getMessages());
        }

        Sessions::addAlertList($this->getMessages());
        Sessions::del(Sessions::CSRF_TOKEN_KEY);
        $redirect = new Redirection();
        $redirect::redirect('admin');
    }

    /**
     * @param array $post
     */
    public function delete(array $post)
    {
        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            if (key_exists('rank_fallback', $post) === false) {
                $this->setMessage('No fallback rank was provided', MessageType::ERROR);
            }

            if (empty($this->getMessages()) === true) {
                $rank = new Rank($post);
                $fallbackRank = new Rank();
                $fallbackRank->setId($post['rank_fallback']);

                $dbConnectionHandler = new DatabaseLayer();
                $dbConnectionHandler->useCRUD();
                $dbInstance = $dbConnectionHandler->getDbHandler();
                $rankManager = new RankManager();
                $rankManager->del($dbInstance, $rank, $fallbackRank, new UserManager());
                $this->setMessages($rankManager->getMessages());
            }
        }

        Sessions::addAlertList($this->getMessages());
        Sessions::del(Sessions::CSRF_TOKEN_KEY);
        $redirect = new Redirection();
        $redirect::redirect('admin');
    }
}
