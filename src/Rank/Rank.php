<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 02/09/2016
 * Time: 15:36
 */

namespace App\Rank;

use App\Traits\FormatChecks;
use Core\Package\AbstractObject;

/**
 * Class Rank
 * @package App\Objects
 */
class Rank extends AbstractObject
{
    use FormatChecks;

    protected $id = null;
    protected $name = null;
    protected $privilegeLevel = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrivilegeLevel()
    {
        return $this->privilegeLevel;
    }

    /**
     * @param mixed $idRank
     * @return bool
     */
    public function setId($idRank)
    {
        if ($this->isPositiveInteger($idRank) === true) {
            $this->id = intval($idRank);
            return true;
        }
        return false;
    }

    /**
     * @param mixed $name
     * @return bool
     */
    public function setName($name)
    {
        if ($this->isName($name) === true) {
            $this->name = $name;
            return true;
        }
        return false;
    }

    /**
     * @param mixed $privilegeLevel
     * @return bool
     */
    public function setPrivilegeLevel($privilegeLevel)
    {
        if ($this->isPositiveInteger($privilegeLevel) === true) {
            $this->privilegeLevel = intval($privilegeLevel);
            return true;
        }
        return false;
    }
}
