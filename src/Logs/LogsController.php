<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 25/10/2016
 * Time: 18:03
 */

namespace App\Logs;

use App\Renderer\TwigRenderer;
use App\Sessions\Sessions;
use Core\Log\Logger;
use Core\Message\MessageType;
use Core\Redirection\Redirection;

/**
 * Class LogsController
 * @package App\Controller
 */
class LogsController
{
    /**
     *  List all logs
     */
    public function listLogs()
    {
        /*
         *  Structure of each entry :
         *  [
         *  'fileName' => $fileName Can be customized
         *     'filePath' => $filePath Used in view and application to point to the correct document
         *  ]
         */
        $files = [];
        $path = Logger::BASE_LOGGING_DIR;
        $pathFinalSlashRm = rtrim(Logger::BASE_LOGGING_DIR, '\ /');

        $directoryIterator = new \RecursiveDirectoryIterator($path);
        $iteratorIterator = new \RecursiveIteratorIterator($directoryIterator);
        foreach ($iteratorIterator as $splFileInfo) {
            if ($splFileInfo->getFilename() === '..' || $splFileInfo->getFilename() === '.') {
                continue;
            }
            $relevantPath = str_replace(
                $pathFinalSlashRm,
                '',
                $splFileInfo->getPathname()
            );
            $relevantPath = str_replace('\\', DS, $relevantPath);
            $relevantPath = ltrim($relevantPath, DS);
            $files[] = [
                'fileName' => $splFileInfo->getFilename(),
                'filePath' => $relevantPath
            ];
        }

        $vars = ['logs' => $files];

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setVars($vars);
        $renderer->setView('list');
        $renderer->render();
    }

    /**
     * @param $filePath
     */
    public function show($filePath)
    {
        if (file_exists(Logger::BASE_LOGGING_DIR . $filePath) === false) {
            Sessions::addAlert('The log file you want to view does not exist.', MessageType::ERROR);
            $redirect = new Redirection();
            $redirect::redirect('admin' . DS . 'logs' . DS);
        }
        $fileContent = file_get_contents(Logger::BASE_LOGGING_DIR . $filePath);
        $vars = [
            'content'   => $fileContent,
            'name'      => $filePath];

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setVars($vars);
        $renderer->setView('show');
        $renderer->render();
    }
}
