<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 23/02/2016
 * Time: 18:56
 */

namespace App\Article;

use App\Category\CategoryManager;
use App\DBLayer\DatabaseLayer;
use App\Renderer\TwigRenderer;
use App\Sessions\Sessions;
use App\Traits\FormatChecks;
use Core\Generate\Generate;
use Core\Globals\Post;
use Core\Package\AbstractController;
use Core\Redirection\Redirection;

/**
 * Class ArticlesController
 * @package App\Article
 */
class ArticleController extends AbstractController
{
    use FormatChecks;

    // GET REQUESTS.
    /**
     * Show article currently selected
     * @param $idArticle
     * @param null $slug
     */
    public function show($idArticle, $slug = null)
    {
        echo 'Je suis l\'article ' . $idArticle;
        if (is_null($slug) === false) {
            echo '. Mon slug est : ' . $slug;
        }
        echo '<br>';

        $databaseLayer = new DatabaseLayer();
        $databaseLayer->useRead();
        $dbInstance = $databaseLayer->getDbHandler();
        $articleManager = new ArticleManager();
        $articleManager->getArticleById($dbInstance, $idArticle);

        var_dump($articleManager->getResults());
    }

    /**
     * @param int $page
     */
    public function listArticles($page = 1)
    {
        $elementPerPage = 1;
        $vars = $this->getArticleListData(intval($page), $elementPerPage);

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setVars($vars);
        $renderer->setView('listArticles');
        $renderer->render();
    }

    // Methods used in Administration context.
    /**
     * @param int $page
     */
    public function listArticlesForAdmin($page = 1)
    {
        $elementPerPage = 1;
        $vars = $this->getArticleListData(intval($page), $elementPerPage);

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setVars($vars);
        $renderer->setView('adminList');
        $renderer->render();
    }

    /**
     *  Show new article view / form
     */
    public function addArticle()
    {
        $csrfToken = Generate::csrfToken();
        Sessions::set(Sessions::CSRF_TOKEN_KEY, $csrfToken);

        $databaseLayer = new DatabaseLayer();
        $databaseLayer->useRead();
        $dbInstance = $databaseLayer->getDbHandler();
        $categoryManager = new CategoryManager();
        $categoryManager->getList($dbInstance);
        $categories = $categoryManager->getResults();

        $vars = [
            'categories' => $categories,
            'csrfToken' => $csrfToken
        ];

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setVars($vars);
        $renderer->setView('addArticle');
        $renderer->render();
    }

    /**
     * Show edit article form / view
     *
     * @param $idArticle
     */
    public function editArticle($idArticle)
    {
        $csrfToken = Generate::csrfToken();
        Sessions::set(Sessions::CSRF_TOKEN_KEY, $csrfToken);

        $databaseLayer = new DatabaseLayer();
        $databaseLayer->useRead();
        $dbInstance = $databaseLayer->getDbHandler();
        $categoryManager = new CategoryManager();
        $categoryManager->getList($dbInstance);
        $categories = $categoryManager->getResults();

        $articleManager = new ArticleManager();
        $status = $articleManager->getById($dbInstance, $idArticle, \PDO::PARAM_INT);

        if ($status === false) {
            Sessions::addAlert('Cet article n\'existe pas.');
            $redirect = new Redirection();
            $redirect::redirect('admin/articles/');
        }
        $articleData = $articleManager->getResults();
        $article = new Article($articleData);

        $vars = [
            'article'    => $article,
            'categories' => $categories,
            'csrfToken'  => $csrfToken
        ];

        $renderer = new TwigRenderer(__DIR__ . DS . 'views' . DS);
        $renderer->setVars($vars);
        $renderer->setView('editArticle');
        $renderer->render();
    }

    // POST REQUESTS.
    /**
     * Insert article into DB
     * @param array $post
     */
    public function addArticleToDB(array $post)
    {
        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            var_dump($post);
        }
    }

    /**
     *  Perform edit on article into DB
     * @param array $post
     */
    public function editArticleToDB(array $post)
    {
        $csrfPassed = $this->isCsrfProtectionPassed(
            Post::getKey(Post::CSRF_TOKEN_KEY),
            Sessions::getKey(Sessions::CSRF_TOKEN_KEY)
        );

        if ($csrfPassed === true) {
            var_dump($post);
        }
    }

    /**
     * @param int $page
     * @param int $elementPerPage
     * @return array
     */
    private function getArticleListData(int $page, int $elementPerPage)
    {
        $databaseLayer = new DatabaseLayer();
        $databaseLayer->useRead();
        $dbInstance = $databaseLayer->getDbHandler();
        $articleManager = new ArticleManager();

        $articleManager->nbRows($dbInstance);
        // Calculate totalPages.
        $totalPages = intval(ceil($articleManager->getNbRows() / $elementPerPage));

        $articleManager->getList(
            $dbInstance,
            $page,
            $elementPerPage
        );
        $articles = $articleManager->getResults();
        $vars = [
            'articles'      => $articles,
            'page'          => $page,
            'totalPages'    => $totalPages
        ];
        return $vars;
    }
}
