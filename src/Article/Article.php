<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 26/02/2016
 * Time: 14:18
 */

namespace App\Article;

use App\Traits\FormatChecks;
use Core\Package\AbstractObject;
use Core\Traits\TextFormatting;

/**
 * Class Article
 * @package App\Objects
 */
class Article extends AbstractObject
{
    use TextFormatting, FormatChecks;

    protected $id;
    protected $title;
    protected $content;
    protected $slug;
    protected $tags;
    protected $categoryId;
    protected $categoryName;
    protected $author;
    protected $datePost;
    protected $dateEdit;
    protected $contentShorted = false;

    // GETTERS.
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param int $length
     * @return string
     */
    public function getShortContent(int $length)
    {
        if (strlen($this->content) >= $length) {
            $this->contentShorted = true;
            return $this->textShortage($this->content, $length);
        }
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @return mixed
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return mixed
     */
    public function getDatePost()
    {
        return $this->datePost;
    }

    /**
     * @return mixed
     */
    public function getDateEdit()
    {
        return $this->dateEdit;
    }

    /**
     * @return mixed
     */
    public function isContentShorted()
    {
        return $this->contentShorted;
    }

    // SETTERS.
    /**
     * @param $idArticle
     * @return bool
     */
    public function setId($idArticle)
    {
        if ($this->isPositiveInteger($idArticle) === true) {
            $this->id = intval($idArticle);
            return true;
        }
        return false;
    }

    /**
     * @param string $title
     * @return bool
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return true;
    }

    /**
     * @param string $content
     * @return bool
     */
    public function setContent(string $content)
    {
        $this->content = $content;
        return true;
    }

    /**
     * @param string $slug
     * @return bool
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
        return true;
    }

    /**
     * @param $tags
     * @return bool
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return true;
    }

    /**
     * @param mixed $categoryId
     * @return bool
     */
    public function setCategoryId($categoryId)
    {
        if ($this->isPositiveInteger($categoryId) === true) {
            $this->categoryId = $categoryId;
            return true;
        }
        return false;
    }

    /**
     * @param $categoryName
     * @return bool
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;
        return true;
    }

    /**
     * @param $author
     * @return bool
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return true;
    }

    /**
     * @param $datePost
     * @return bool
     */
    public function setDatePost($datePost)
    {
        $this->datePost = $datePost;
        return true;
    }

    /**
     * @param $dateEdit
     * @return bool
     */
    public function setDateEdit($dateEdit)
    {
        $this->dateEdit = $dateEdit;
        return true;
    }
}
