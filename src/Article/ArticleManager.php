<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/02/2016
 * Time: 16:50
 */

namespace App\Article;

use Core\Database\JoinClauseTypes;
use Core\Package\AbstractManager;
use PDO;

/**
 * Class ArticlesManager
 * @package App\Article
 */
class ArticleManager extends AbstractManager
{
    /**
     * @param PDO $dbInstance
     * @param $idValue
     * @param int $type
     * @return bool|true
     */
    public function getArticleById(PDO $dbInstance, $idValue, int $type = PDO::PARAM_INT)
    {
        $this->selectSetUp();
        $this->where('article.id', $idValue, $type);
        return $this->select($dbInstance);
    }

    /**
     * @param PDO $dbInstance
     * @param int $page
     * @param int $elementsPerPage
     * @return bool|true
     */
    public function getList(PDO $dbInstance, int $page, int $elementsPerPage)
    {
        $this->selectSetUp();
        $this->order('article.id', 'DESC');
        return $this->getListFromPage($dbInstance, new Article(), $page, $elementsPerPage);
    }

    /**
     *
     */
    public function addArticle()
    {
    }

    private function selectSetUp()
    {
        $this->addColumn('*');
        $this->addColumn('article.id', null, 'id');
        $this->addColumn('category.id', null, 'category_id');
        $this->join(JoinClauseTypes::LEFT, 'category', 'category_id', 'id');
    }
}
