<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 16/12/2016
 * Time: 21:48
 */

namespace App\DBLayer;

use Core\Database\ConnectionLayer;
use Core\Database\DatabaseDbms;

/**
 * Class DatabaseLayer
 * @package App\DBLayer
 */
class DatabaseLayer extends ConnectionLayer
{
    /**
     * Set basic references
     */
    protected function setBasicReference()
    {
        $this->setDbName(DB_NAME);
        $this->setHost(DB_HOST);
        $this->setDbms(DatabaseDbms::MYSQL);
    }

    /**
     * Set READ Credential
     */
    public function useRead()
    {
        $this->setBasicReference();
        $this->setUser(DB_READ_LOGIN);
        $this->setPassword(DB_READ_PWD);
        return $this->connect();
    }

    /**
     * Set CRU credentials
     */
    public function useCRU()
    {
        $this->setBasicReference();
        $this->setUser(DB_CRU_LOGIN);
        $this->setPassword(DB_CRU_PWD);
        return $this->connect();
    }

    /**
     * Set CRUD credentials
     */
    public function useCRUD()
    {
        $this->setBasicReference();
        $this->setUser(DB_CRUD_LOGIN);
        $this->setPassword(DB_CRUD_PWD);
        return $this->connect();
    }
}
