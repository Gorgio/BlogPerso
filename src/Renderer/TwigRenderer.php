<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 03/02/2017
 * Time: 22:55
 */

namespace App\Renderer;

use App\Sessions\Sessions;
use Core\Renderer\AbstractRenderer;
use Core\System\System;

/**
 * Class NewTwigRenderer
 * @package App\Renderer
 */
class TwigRenderer extends AbstractRenderer
{
    const DIR_COMPILATION_CACHE = 'temp' . DS . 'render' . DS;

    protected $directory = '';
    protected $fileExtension = '';
    protected $name = '';
    protected $vars = [];

    /**
     * NewTwigRenderer constructor.
     * @param string $directory
     */
    public function __construct(string $directory)
    {
        $this->directory = $directory;
    }

    /**
     * @param string $name
     * @param string $fileExtension
     */
    public function setView(string $name, string $fileExtension = '.view.twig')
    {
        $this->fileExtension = $fileExtension;
        $this->name = $name;
    }

    /**
     * Renders the view specified
     */
    public function render()
    {
        $loader = new \Twig_Loader_Filesystem([$this->getDirectory(), self::DIR_VIEWS]);
        $loader->addPath(self::DIR_VIEWS . 'errors' . DS, 'errors');
        $loader->addPath(self::DIR_VIEWS . 'layouts' . DS, 'layouts');
        $loader->addPath(self::DIR_VIEWS . 'snippets' . DS, 'snippets');

        if (System::isDevelopmentEnvironment() === true) {
            $params = ['debug' => true];
        } else {
            $params['cache'] = self::DIR_COMPILATION_CACHE;
        }

        $twig = new \Twig_Environment($loader, $params);
        $twig->addExtension(new \Twig_Extension_Debug());
        $template = $twig->load($this->getName() . $this->getFileExtension());

        $display = $template->render($this->getVars());

        $alertDisplay = Sessions::getHTMLAlertDisplay();

        $display = preg_replace(
            '#%%%ALERT_DISPLAY%%%#',
            $alertDisplay,
            $display,
            1,
            $count
        );

        if ($count > 0) {
            Sessions::del("alerts");
        }

        // Replace href & action roots by System:$webRoot.
        $display = preg_replace_callback(
            '#href="(/){1}#',
            function () {
                return 'href="' . System::$webRoot;
            },
            $display
        );

        $display = preg_replace_callback(
            '#action="(/){1}#',
            function () {
                return 'action="' . System::$webRoot;
            },
            $display
        );

        echo $display;
    }

    /**
     * @return string
     */
    public function getFileExtension(): string
    {
        return $this->fileExtension;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
