<?php
if (stream_resolve_include_path('environment.local.php') !== false) {
    include_once 'environment.local.php';
    include_once 'credentials.db.local.php';
} else {
    include_once 'credentials.db.php';
}
