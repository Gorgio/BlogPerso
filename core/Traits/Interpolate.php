<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 07/01/2017
 * Time: 23:46
 */

namespace Core\Traits;

/**
 * Class Interpolate
 * @package Core\Traits
 */
trait Interpolate
{
    /**
     * Interpolates context values into a string message's placeholders.
     * @param string $message
     * @param array $context
     * @return string
     */
    public function interpolate(string $message, array $context = []): string
    {
        // Build a replacement array with braces around the context keys.
        $replace = [];
        foreach ($context as $key => $val) {
            if ($val instanceof \Exception) {
                if ($key !== 'exception') {
                    throw new \InvalidArgumentException(
                        'Context value is an instance of Exception but no Exception key found.'
                    );
                }
                $replace['{' . $key . '}'] = $val;
            } elseif (is_array($val) === false
                && (is_object($val) === false
                    || method_exists($val, '__toString') === true
                )
            ) {
                $replace['{' . $key . '}'] = $val;
            }
            /*
             * First check if the value given is an Exception, check if there is a exception placeholder.
             * If not throw an InvalidArgumentException
             * If the value given is not a string but has a __toString method use it to interpolate the message.
             */
        }

        // Interpolate replacement values into the message and return.
        return strtr($message, $replace);
    }
}
