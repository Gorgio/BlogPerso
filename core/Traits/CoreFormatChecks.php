<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 03/04/2016
 * Time: 14:15
 */

namespace Core\Traits;

/**
 * Class CoreFormatChecks
 * @package Core\Traits
 */
trait CoreFormatChecks
{
    /**
     * @param int $intToTest
     * @return bool
     * @internal param bool $strict
     */
    protected final function isPositiveInteger(int $intToTest)
    {
        if ($intToTest >= 0) {
            return true;
        }
        return false;
    }

    /**
     * @param int $intToTest
     * @return bool
     */
    protected final function isStrictPositiveInteger(int $intToTest)
    {
        if ($intToTest === 0) {
            return false;
        }

        return $this->isPositiveInteger($intToTest);
    }

    /**
     * @param string $value
     * @return bool
     */
    protected function isMail(string $value)
    {
        if (is_string(filter_var($value, FILTER_VALIDATE_EMAIL)) === true) {
            return true;
        }
        return false;
    }
}
