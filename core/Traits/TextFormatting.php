<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 26/03/2016
 * Time: 20:36
 */

namespace Core\Traits;

/**
 * Class TextFormatting
 * @package Core\Traits
 */
trait TextFormatting
{
    /**
     * Generates an extract of the text for a given length
     *
     * @param string $text   Original text to truncate
     * @param int    $length Length of the extract
     *
     * @return string
     */
    public function textShortage(string $text, int $length)
    {
        if (strlen($text) >= $length) {
            // Get an extract of the given length form the original text provided.
            $extract = substr($text, 0, $length);
            // Finds position of last space character from the extract.
            $space = strrpos($extract, ' ');
            // Get the new extract where it's stops before the last space character.
            $text = substr($extract, 0, $space).' ...';
        }

        return $text;
    }
}
