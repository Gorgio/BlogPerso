<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 08/01/2017
 * Time: 11:42
 */

namespace Core\Message;

/**
 * Class MessageType
 * @package Core\Message
 */
class MessageType
{
    const APP_ERROR = 'error-app';
    const DEFAULT   = 'default';
    const ERROR     = 'error';
    const INFO      = 'info';
    const SUCCESS   = 'success';
    const WARNING   = 'warning';
}
