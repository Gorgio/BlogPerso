<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 09/11/2016
 * Time: 19:08
 */

namespace Core\Message;

use Core\Traits\Interpolate;
use Psr\Log\LoggerInterface;

/**
 * Class Message
 * @package Core\Message
 */
trait Message
{
    use Interpolate;

    protected $messages = [];

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param array $messages
     */
    protected function setMessages(array $messages)
    {
        foreach ($messages as $messageData) {
            $message = '';
            $type = MessageType::ERROR;
            $context = [];
            if (is_array($messageData) === true) {
                if (isset($messageData['message']) === true) {
                    $message = $messageData['message'];
                }
                if (isset($messageData['type']) === true) {
                    $type = $messageData['type'];
                }
                if (isset($messageData['context']) === true) {
                    $context = $messageData['context'];
                }
            }
            $this->setMessage($message, $type, $context);
        }
    }

    /**
     * @param string $message
     * @param string $type
     * @param array $context
     */
    protected function setMessage(string $message, string $type = MessageType::ERROR, array $context = [])
    {
        $message = $this->interpolate($message, $context);
        $this->messages[] = [
            'message'   => ucfirst($message),
            'type'      => $type
        ];
    }

    /**
     * @param LoggerInterface $logger
     * @param $logLevel
     * @internal param array $messages
     */
    protected final function logMessages(LoggerInterface $logger, $logLevel)
    {
        foreach ($this->getMessages() as $messageData) {
            $logger->log($logLevel, $messageData['message']);
        }
    }
}
