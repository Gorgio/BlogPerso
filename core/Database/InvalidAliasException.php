<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/01/2017
 * Time: 13:49
 */

namespace Core\Database;

/**
 * Class InvalidAliasException
 * @package Core\Database
 */
class InvalidAliasException extends DatabaseException
{
    const GENERAL = 'Alias: `{alias}` is invalid.';
    const SELECT = 'Alias: `{alias}` provided for SELECT statement is invalid.';

    /**
     * InvalidAliasException constructor.
     * @param string $message
     * @param string $alias
     * @param int $code
     * @param null $previous
     */
    public function __construct($message, string $alias, $code = 0, $previous = null)
    {
        $context = $this->interpolate($message, ['alias' => $alias]);
        parent::__construct($message, $context, $code, $previous);
    }
}
