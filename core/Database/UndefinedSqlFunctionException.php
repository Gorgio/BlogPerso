<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/01/2017
 * Time: 14:24
 */

namespace Core\Database;

/**
 * Class UndefinedSqlFunctionException
 * @package Core\Database
 */
class UndefinedSqlFunctionException extends DatabaseException
{
    const AGGREGATE = 'Aggregate function: `{function}` provided is undefined.';
    const HAVING = 'Aggregate function: `{function}` provided to HAVING clause is undefined.';
    const SELECT = 'Function: `{function}` provided to SELECT statement is undefined.';

    /**
     * UndefinedSqlFunctionException constructor.
     * @param string $message
     * @param string $function
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message, string $function, $code = 0, \Exception $previous = null)
    {
        $context = ['function' => $function];
        parent::__construct($message, $context, $code, $previous);
    }
}
