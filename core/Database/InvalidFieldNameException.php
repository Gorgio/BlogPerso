<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/01/2017
 * Time: 16:47
 */

namespace Core\Database;

/**
 * Class InvalidFieldNameException
 * @package Core\Database
 */
class InvalidFieldNameException extends DatabaseException
{
    const GENERAL = 'Field: `{field}` to modify in statement is invalid.';

    /**
     * InvalidFieldNameException constructor.
     * @param string $message
     * @param string $field
     * @param int $code
     * @param null $previous
     */
    public function __construct(string $message, string $field, $code = 0, $previous = null)
    {
        $context = ['field' => $field];
        parent::__construct($message, $context, $code, $previous);
    }
}
