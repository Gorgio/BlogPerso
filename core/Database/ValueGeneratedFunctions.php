<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 25/01/2017
 * Time: 00:03
 */

namespace Core\Database;

use Core\Enum\BasicEnum;

/**
 * Class ValueGeneratedFunctions
 * @package Core\Database\Errors
 */
class ValueGeneratedFunctions extends BasicEnum
{
    const NOW = 'NOW';
    const RAND = 'RAND';
}
