<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 24/01/2017
 * Time: 23:50
 */

namespace Core\Database;

use Core\Enum\BasicEnum;

/**
 * Class ComparaisonOperators
 * @package Core\Database
 */
class ComparaisonOperators extends BasicEnum
{
    const DIFFERENT = '<>';
    const EQUAL = '=';
    const LESS_THAN = '<';
    const LESS_THAN_OR_EQUAL = '<=';
    const MORE_THAN = '>';
    const MORE_THAN_OR_EQUAL = '>=';
}
