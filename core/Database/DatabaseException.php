<?php

namespace Core\Database;

use Core\Exception\ExceptionWithContext;

/**
 * Class DatabaseException
 * @package Core\Database
 */
abstract class DatabaseException extends ExceptionWithContext
{

}
