<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 24/01/2017
 * Time: 23:28
 */

namespace Core\Database;

use Core\Enum\BasicEnum;

/**
 * Class AggregateFunctionTypes
 * @package Core\Database
 */
class AggregateFunctionTypes extends BasicEnum
{
    const AVERAGE = 'AVG';
    const COUNT = 'COUNT';
    const MAX = 'MAX';
    const MIN = 'MIN';
    const SUM = 'SUM';
}
