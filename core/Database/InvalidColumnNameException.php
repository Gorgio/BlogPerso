<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/01/2017
 * Time: 12:48
 */

namespace Core\Database;

/**
 * Class InvalidColumnNameException
 * @package Core\Database
 */
class InvalidColumnNameException extends DatabaseException
{
    const GENERAL = 'Column: `{column}` provided is invalid.';
    const GROUP_BY = 'GROUP BY clause column: `{column}` provided is invalid';
    const HAVING = 'HAVING clause column: `{column}` provided is invalid';
    const JOIN_INITIAL = 'Initial Join Column: `{column}` provided is invalid.';
    const JOIN_FK = 'Foreign Key Join Column: `{column}` provided is invalid.';
    const ORDER_BY = 'ORDER BY clause column: `{column}` provided is invalid';
    const PREFIX = 'Prefix for columns: `{column}` provided is invalid';
    const SELECT = 'Column: `{column}` provided to SELECT statement is invalid.';
    const WHERE = 'WHERE clause column: `{column}` provided is invalid';

    /**
     * InvalidColumnNameException constructor.
     * @param string $message
     * @param string $column
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct(string $message, string $column, $code = 0, \Exception $previous = null)
    {
        $context = ['column' => $column];
        parent::__construct($message, $context, $code, $previous);
    }
}
