<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/01/2017
 * Time: 15:25
 */

namespace Core\Database;

/**
 * Class InvalidFieldDataException
 * @package Core\Database
 */
class InvalidFieldDataException extends DatabaseException
{
    const DATA_MISSING = 'Info field to bind isn\'t an array. Data missing to proceed.';
    const PARAM_TYPE = 'Param type `{type}` is invalid or not supported.';
    const TYPE_MISSING = 'Param Type `{type}` is missing';
    const VALUE_MISSING = 'Value field missing for field `{field}` to bind.';
}
