<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/01/2017
 * Time: 13:30
 */

namespace Core\Database;

/**
 * Class InvalidTableNameException
 * @package Core\Database
 */
class InvalidTableNameException extends DatabaseException
{
    const FROM = 'Table provided for FROM clause: `{table}` is invalid.';
    const JOIN = 'Table provided for JOIN clause: `{table}` is invalid.';

    /**
     * DatabaseException constructor.
     * @param string $message
     * @param string $tableName
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message, string $tableName, $code = 0, \Exception $previous = null)
    {
        $context = ['table' => $tableName];
        parent::__construct($message, $context, $code, $previous);
    }
}
