<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/01/2017
 * Time: 15:09
 */

namespace Core\Database;

/**
 * Class UndefinedSqlJoinTypeException
 * @package Core\Database
 */
class UndefinedSqlJoinTypeException extends DatabaseException
{
    const GENERAL = 'Join type: `{type}` provided for JOIN clause is undefined.';

    /**
     * UndefinedSqlJoinTypeException constructor.
     * @param string $message
     * @param string $type
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($message, string $type, $code = 0, $previous = null)
    {
        $context = ['type' => $type];
        parent::__construct($message, $context, $code, $previous);
    }
}
