<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 24/01/2017
 * Time: 23:47
 */

namespace Core\Database;

use Core\Enum\BasicEnum;

/**
 * Class CharacterFunctionTypes
 * @package Core\Database
 */
class CharacterFunctionTypes extends BasicEnum
{
    const UPPER_CASE = 'UPPER';
    const LOWER_CASE = 'LOWER';
}
