<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 18/12/2016
 * Time: 14:44
 */

namespace Core\Database;

use Core\Enum\BasicEnum;

/**
 * Class DatabaseDbms
 * @package Core\Database
 */
class DatabaseDbms extends BasicEnum
{
    const MYSQL = 'mysql';
    const POSTGRESQL = 'pgsql';
    const MICROSOFT_SQL_SERVER = 'mssql';
    const ORACLE = 'oci';
    const MS_SQL_SERVER = 'sqlsrv';
    const SQL_AZURE = 'sqlsrv';
    const SQLITE = 'sqlite';
    const SQLITE_2 = 'sqlite2';
}
