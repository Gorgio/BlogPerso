<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 06/11/2016
 * Time: 22:25
 */

namespace Core\Database;

use Core\Log\Logger;
use Core\Message\Message;
use Core\Message\MessageType;
use PDO;
use PDOException;
use PDOStatement;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Class QueryConstruct
 * @package Core\Database
 */
class QueryConstruct
{
    use LoggerAwareTrait, Message;

    private $tableName    = '';
    private $columnPrefix = '';
    private $prefixEnable = false;
    private $debugMode = false;

    private $columns      = [];
    private $fieldsToBind = [];
    private $results;
    // Clauses.
    private $distinct = '';
    private $joinStatement     = '';
    private $whereStatement    = '';
    private $groupByStatement  = '';
    private $havingStatement   = '';
    private $orderByStatement  = '';
    private $limitStatement    = '';

    const REQUEST_DELETE = 'DEL';
    const REQUEST_INSERT = 'ADD';
    const REQUEST_SELECT = 'GET';
    const REQUEST_UPDATE = 'SET';

    /**
     * QueryConstruct constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
        if (is_null($logger) === true) {
            $logger = new Logger();
            $logger->setLoggingDir('database');
            $logger->setFileName('queries');
        }
        $this->setLogger($logger);
        // Set default table name.
        $reflect = new \ReflectionClass(get_called_class());
        $tableName = strtolower(str_replace('Manager', '', $reflect->getShortName()));
        $this->setTableName($tableName);
    }

    /**
     * @param PDO $dbInstance
     * @return bool
     */
    final public function insert(PDO $dbInstance)
    {
        return $this->execute($dbInstance, self::REQUEST_INSERT);
    }

    /**
     * @param PDO $dbInstance
     * @return bool
     */
    final public function deleteFrom(PDO $dbInstance)
    {
        return $this->execute($dbInstance, self::REQUEST_DELETE);
    }

    /**
     * @param PDO $dbInstance
     * @return bool
     */
    final public function select(PDO $dbInstance)
    {
        return $this->execute($dbInstance, self::REQUEST_SELECT);
    }

    /**
     * @param PDO $dbInstance
     * @return bool
     */
    final public function update(PDO $dbInstance)
    {
        return $this->execute($dbInstance, self::REQUEST_UPDATE);
    }

    // EXECUTE QUERIES.
    /**
     * @param PDO $dbInstance
     * @param string $queryType
     * @return bool
     */
    final private function execute(PDO $dbInstance, string $queryType)
    {
        $logLevel = LogLevel::CRITICAL;
        $status = false;
        $statement = null;

        switch ($queryType) {
            case self::REQUEST_INSERT:
                $statement = $this->execInsertInto();
                break;
            case self::REQUEST_DELETE:
                $statement = $this->execDeleteFrom();
                break;
            case self::REQUEST_SELECT:
                $statement = $this->execSelect();
                break;
            case self::REQUEST_UPDATE:
                $statement = $this->execUpdate();
                break;
            default:
                throw new \UnexpectedValueException('Undefined query type `'. $queryType . '` provided.');
                break;
        }
        if ($this->isDebugMode() === true) {
            $this->getLogger()->log(
                LogLevel::DEBUG,
                'Statement: {statement}',
                ['statement' => $statement]
            );
        }
        if (empty($this->fieldsToBind) === false) {
            $query = $dbInstance->prepare($statement);
            $this->parseFieldsToStatement($query);
            $query->execute();
        } else {
            $query = $dbInstance->query($statement);
        }
        if ($query instanceof PDOStatement) {
            try {
                $data = $query->fetchAll();
                $this->setResults($data);
                $logLevel = LogLevel::NOTICE;
                $status = true;
            } catch (PDOException $e) {
                $this->logger->critical(
                    "Statement: {statement}\r\nAn Exception occurred:\r\n{exception}",
                    [
                        'statement' => $statement,
                        'exception' => $e
                    ]
                );
            }
        }

        $this->logMessages($this->getLogger(), $logLevel);

        $this->reset();

        return $status;
    }

    /**
     * @return string
     * @internal
     */
    final private function execInsertInto()
    {
        $statement = 'INSERT INTO ' . $this->getTableName() . ' (';
        $columnList = '';
        foreach ($this->columns as $column) {
            $columnList .= $column .  ',';
        }
        $columnList = rtrim($columnList, ',');
        $statement .= $columnList . ') VALUES (';
        $columnList = '';
        foreach ($this->columns as $column => $fieldToBindAlias) {
            $value = $this->fieldsToBind[$fieldToBindAlias]['value'];
            if (is_string($value) === true && ValueGeneratedFunctions::isValidValue($value) === true) {
                $columnList .= $value . '(),';
                unset($this->fieldsToBind[$fieldToBindAlias]);
            } else {
                $columnList .= ':'. $fieldToBindAlias .',';
            }
        }
        $columnList = rtrim($columnList, ',');
        $statement .= $columnList . ')';

        return $statement;
    }

    /**
     * @return string
     * @internal
     */
    final private function execDeleteFrom()
    {
        $statement = 'DELETE FROM ' . $this->getTableName() . $this->whereStatement;
        return $statement;
    }

    /**
     * @return string
     * @internal
     */
    final private function execSelect()
    {
        $statement = 'SELECT ';
        $statement .= $this->distinct;

        if (empty($this->columns) === false) {
            foreach ($this->columns as $column => $info) {
                if (is_null($info['function']) === false) {
                    $statement .= $info['function'] . '('. $column .')';
                } else {
                    $statement .= $column;
                }

                if ($info['alias'] !== $column) {
                    $statement .= ' AS ' . $info['alias'];
                }
                $statement .= ',';
            }
            $statement = rtrim($statement, ',');
        } else {
            $statement .= '*';
        }

        $statement .= ' FROM ' . $this->getTableName();
        $statement .= $this->joinStatement . $this->whereStatement . $this->groupByStatement .
            $this->havingStatement . $this->orderByStatement . $this->limitStatement;

        return $statement;
    }

    /**
     * @return string
     * @internal
     */
    final private function execUpdate()
    {
        $statement = 'UPDATE ' . $this->getTableName() . ' SET ';
        foreach ($this->columns as $column => $fieldToBindAlias) {
            $statement .= $column . '=';
            $value = $this->fieldsToBind[$fieldToBindAlias]['value'];
            if (is_string($value) === true && ValueGeneratedFunctions::isValidValue($value) === true) {
                $statement .= $value . '(),';
                unset($this->fieldsToBind[$fieldToBindAlias]);
            } else {
                $statement .= ':'. $fieldToBindAlias .',';
            }
        }
        $statement = rtrim($statement, ',');
        $statement .= $this->whereStatement;
        return $statement;
    }

    /** SQL CLAUSES */
    final public function distinct()
    {
        $this->distinct = 'DISTINCT';
    }

    /**
     * @param string $joinType
     * @param string $tableToJoin
     * @param string $initialColumn
     * @param string $fkColumn
     * @return $this
     * @throws InvalidColumnNameException
     * @throws InvalidTableNameException
     * @throws UndefinedSqlJoinTypeException
     */
    final public function join(string $joinType, string $tableToJoin, string $initialColumn = '', string $fkColumn = '')
    {
        $initialColumn = $this->getPrefixedColumn($initialColumn);

        if (JoinClauseTypes::isValidValue($joinType) === false) {
            throw new UndefinedSqlJoinTypeException(UndefinedSqlJoinTypeException::GENERAL, $joinType);
        }
        if ($this->isSqlValidName($tableToJoin) === false) {
            throw new InvalidTableNameException(InvalidTableNameException::JOIN, $tableToJoin);
        }

        $joinStatement = ' ' . $joinType . ' JOIN ' . $tableToJoin;
        if ($joinType !== JoinClauseTypes::NATURAL
            && $joinType !== JoinClauseTypes::CROSS
        ) {
            if ($this->isSqlValidName($initialColumn) === false) {
                throw new InvalidColumnNameException(InvalidColumnNameException::JOIN_INITIAL, $initialColumn);
            }
            if ($this->isSqlValidName($fkColumn) === false) {
                throw new InvalidColumnNameException(InvalidColumnNameException::JOIN_FK, $fkColumn);
            }
            $joinStatement .= ' ON ' . $this->getTableName() . ". $initialColumn = $tableToJoin.$fkColumn";
        }
        $this->joinStatement = $joinStatement;

        return $this;
    }

    /**
     * Allows to do a simple WHERE condition
     * @param string $column
     * @param $value
     * @param int $type
     * @param string $operator
     * @param bool $caseSensitive
     * @param string $alias | set up specific alias for bindValue
     * @return $this
     * @throws InvalidColumnNameException
     * @throws UndefinedSqlComparaisonOperatorException
     */
    final public function where(
        string $column,
        $value,
        int $type = PDO::PARAM_STR,
        string $operator = '=',
        bool $caseSensitive = true,
        string $alias = 'where'
    ) {
        $column = $this->getPrefixedColumn($column);

        if ($this->isSqlValidName($column) === false) {
            throw new InvalidColumnNameException(InvalidColumnNameException::WHERE, $column);
        }
        if (ComparaisonOperators::isValidValue($operator, true) === false) {
            throw new UndefinedSqlComparaisonOperatorException(
                UndefinedSqlComparaisonOperatorException::WHERE,
                $operator
            );
        }
        if ($caseSensitive === true) {
            $whereStatement = ' WHERE UPPER(' . $column . ') ' . $operator . ' UPPER(:' . $alias . ')';
        } else {
            $whereStatement = ' WHERE ' . $column . ' ' . $operator . ' :' . $alias;
        }
        $this->whereStatement = $whereStatement;

        $this->addFieldToBind($alias, $value, $type);

        return $this;
    }

    /**
     * @param string $column
     * @return $this
     * @throws InvalidColumnNameException
     */
    final public function group(string $column)
    {
        $column = $this->getPrefixedColumn($column);

        if ($this->isSqlValidName($column) === false) {
            throw new InvalidColumnNameException(InvalidColumnNameException::GROUP_BY, $column);
        }
        $groupByStatement = ' GROUP BY ' . $column;
        $this->groupByStatement = $groupByStatement;

        return $this;
    }

    /**
     * @param string $aggregateFunction
     * @param string $column
     * @param string $operator
     * @param $value
     * @return $this
     * @throws InvalidColumnNameException
     * @throws UndefinedSqlComparaisonOperatorException
     * @throws UndefinedSqlFunctionException
     */
    final public function having(string $aggregateFunction, string $column, string $operator, $value)
    {
        $column = $this->getPrefixedColumn($column);

        if (AggregateFunctionTypes::isValidValue($aggregateFunction) === false) {
            throw new UndefinedSqlFunctionException(UndefinedSqlFunctionException::HAVING, $aggregateFunction);
        }
        if ($this->isSqlValidName($column) === false) {
            throw new InvalidColumnNameException(InvalidColumnNameException::HAVING, $column);
        }
        if (ComparaisonOperators::isValidValue($operator, true) === false) {
            throw new UndefinedSqlComparaisonOperatorException(
                UndefinedSqlComparaisonOperatorException::HAVING,
                $operator
            );
        }
        $havingStatement = ' HAVING ' . $aggregateFunction .
            '(' . $column . ') ' . $operator . ' ' . $value;
        $this->havingStatement = $havingStatement;

        return $this;
    }

    /**
     * @param string $column
     * @param string $order = {ASC|DESC}
     * @return $this
     * @throws InvalidColumnNameException
     */
    final public function order(string $column, string $order = 'ASC')
    {
        $column = $this->getPrefixedColumn($column);
        if ($this->isSqlValidName($column) === false) {
            throw new InvalidColumnNameException(InvalidColumnNameException::ORDER_BY, $column);
        }
        $sort = 'ASC';
        if ($order === 'DESC' || $order === 'desc') {
            $sort = 'DESC';
        }

        $orderByStatement = ' ORDER BY ' . $column . ' ' . $sort;

        if (empty($this->orderByStatement) === false) {
            $orderByStatement = $this->orderByStatement . ', ' . $column . ' ' . $sort;
        }

        $this->orderByStatement = $orderByStatement;
        return $this;
    }

    /**
     * @param int $nbRows
     * @param int $offset
     * @return $this
     */
    final public function limit(int $nbRows, int $offset = 0)
    {
        $limit = ' LIMIT ' . $nbRows;
        if ($offset > 0) {
            $limit .= ' OFFSET ' . $offset;
        }
        $this->limitStatement = $limit;
        return $this;
    }

    /** $COLUMN & $FILED */
    /**
     * Add a column to select
     * @param $column
     * @param $function
     * @param $alias
     * @return $this
     * @throws InvalidAliasException
     * @throws InvalidColumnNameException
     * @throws UndefinedSqlFunctionException
     */
    final public function addColumn($column, $function = null, $alias = null)
    {
        if ($column !== '*' && $column !== 1) {
            $column = $this->getPrefixedColumn($column);
        }

        if ($this->isSqlValidColumn($column) === false) {
            throw new InvalidColumnNameException(InvalidColumnNameException::SELECT, $column);
        }

        if (is_null($function) === false
            && AggregateFunctionTypes::isValidValue($function) === false
            && CharacterFunctionTypes::isValidValue($function) === false
        ) {
            throw new UndefinedSqlFunctionException(UndefinedSqlFunctionException::SELECT, $function);
        }
        if (is_null($alias) === true) {
            $alias = $column;
        }
        if ($this->isSqlValidColumn($alias) === false) {
            throw new InvalidAliasException(InvalidAliasException::SELECT, $alias);
        }

        $this->columns[$column] = [
            'alias'     => $alias,
            'function'  => $function
        ];

        return $this;
    }

    /**
     * Add a field to add/edit
     *
     * @param string $column
     * @param $value
     * @param int $type
     * @param $alias @only for Edit
     * @return $this
     * @throws InvalidAliasException
     * @throws InvalidFieldNameException
     */
    final public function addField(string $column, $value, int $type = PDO::PARAM_STR, $alias = null)
    {
        $column = $this->getPrefixedColumn($column);

        if (is_null($alias) === true) {
            $alias = $column;
        }
        if ($this->isSqlValidName($column) === false) {
            throw new InvalidFieldNameException(InvalidFieldNameException::GENERAL, $column);
        }
        if ($this->isSqlValidName($alias) === false) {
            throw new InvalidAliasException(InvalidAliasException::GENERAL, $alias);
        }

        $this->columns[$column] = $alias;
        $this->addFieldToBind($alias, $value, $type);

        return $this;
    }

    /** INTERNAL FUNCTIONS */
    /**
     * @param string $alias
     * @param $value
     * @param int $type
     * @internal
     */
    final private function addFieldToBind(string $alias, $value, int $type = PDO::PARAM_STR)
    {
        if (key_exists($alias, $this->fieldsToBind) === true) {
            $this->setMessage(
                'Alias: ` ' . $alias . '` to bind is already present and is going to be rewritten.',
                MessageType::ERROR
            );
        }
        $info = [
            'value' => $value,
            'type'  => $type,
        ];

        $this->fieldsToBind[$alias] = $info;
    }

    /**
     * @param string $name
     * @return bool
     */
    final protected function isSqlValidName(string $name) : bool
    {
        if (preg_match('#^[a-z_][a-z0-9_.]{1,}$#', $name) === 1) {
            return true;
        }
        return false;
    }

    /**
     * @param $column
     * @return bool
     */
    final protected function isSqlValidColumn($column) : bool
    {
        if ($this->isSqlValidName($column) === false && $column !== '*' && $column !== 1) {
            return false;
        }
        return true;
    }

    /**
     * @param PDOStatement $statement
     * @throws InvalidFieldDataException
     * @internal
     */
    final private function parseFieldsToStatement(PDOStatement $statement)
    {
        $fields = $this->fieldsToBind;

        foreach ($fields as $field => $fieldData) {
            if (is_array($fieldData) === false) {
                throw new InvalidFieldDataException(InvalidFieldDataException::DATA_MISSING);
            }
            if (isset($fieldData['value']) === false) {
                throw new InvalidFieldDataException(InvalidFieldDataException::VALUE_MISSING, ['field' => $field]);
            }
            if (isset($fieldData['type']) === false) {
                throw new InvalidFieldDataException(
                    InvalidFieldDataException::TYPE_MISSING,
                    ['type' => $fieldData['type']]
                );
            }
            $paramType = $fieldData['type'];
            if (ParamTypes::isValidValue($paramType) === false) {
                throw new InvalidFieldDataException(InvalidFieldDataException::PARAM_TYPE, ['type' => $paramType]);
            }

            $value = $fieldData['value'];
            $statement->bindValue($field, $value, $paramType);
        }
    }

    /**
     * @param $column
     * @return string
     * @internal
     */
    final private function getPrefixedColumn($column)
    {
        if ($this->isPrefixEnable() === true && is_null($this->getColumnPrefix()) === false) {
            $column = $this->getColumnPrefix() . '_' . $column;
        }
        return $column;
    }

    final private function reset()
    {
        $this->columns = [];
        $this->fieldsToBind = [];
        $this->distinct = '';
        $this->joinStatement = '';
        $this->whereStatement   = '';
        $this->groupByStatement = '';
        $this->havingStatement  = '';
        $this->orderByStatement = '';
        $this->limitStatement   = '';
    }

    /**
     *  If only one result is expected, brings the result nested in the $result array to the root of $result
     */
    final protected function singleOutResults()
    {
        if (is_array($this->getResults()) === true && count($this->getResults()) === 1) {
            $this->setResults($this->getResults()[0]);
        }
    }

    /**
     * @return string
     * @internal
     */

    /** $GETTERS & SETTERS */
    final private function getColumnPrefix(): string
    {
        return $this->columnPrefix;
    }
    /**
     * @param string $columnPrefix
     * @return $this
     * @throws InvalidColumnNameException
     */
    final protected function setColumnPrefix(string $columnPrefix)
    {
        if ($this->isSqlValidName($columnPrefix) === false) {
            throw new InvalidColumnNameException(InvalidColumnNameException::PREFIX, $columnPrefix);
        }
        $this->columnPrefix = $columnPrefix;
        return $this;
    }

    /**
     * @return boolean
     */
    private function isDebugMode(): bool
    {
        return $this->debugMode;
    }

    /**
     * @param boolean $debugMode
     */
    public function setDebugMode(bool $debugMode)
    {
        $this->debugMode = $debugMode;
    }
    /**
     * @return LoggerInterface
     */
    final protected function getLogger()
    {
        return $this->logger;
    }
    /**
     * @return bool
     * @internal
     */
    final private function isPrefixEnable() : bool
    {
        return $this->prefixEnable;
    }
    /**
     * @param boolean $prefixEnable
     */
    final protected function setPrefixEnable(bool $prefixEnable)
    {
        $this->prefixEnable = $prefixEnable;
    }
    /**
     * @return mixed
     */
    final public function getResults()
    {
        return $this->results;
    }
    /**
     * @param mixed $results
     */
    final protected function setResults($results)
    {
        $this->results = $results;
    }
    /**
     * @return string
     * @internal
     */
    final private function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     * @return $this
     * @throws InvalidTableNameException
     */
    final protected function setTableName(string $tableName)
    {
        if ($this->isSqlValidName($tableName) === false) {
            throw new InvalidTableNameException(InvalidTableNameException::FROM, $tableName);
        }
        $this->tableName = $tableName;
        return $this;
    }
}
