<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/01/2017
 * Time: 16:33
 */

namespace Core\Database;

/**
 * Class InvalidDbInstanceParamException
 * @package Core\Database
 */
class InvalidDbInstanceParamException extends DatabaseException
{
    const USER_MISSING     = 'No `user` defined.';
    const PASSWORD_MISSING = 'No password provided.';
    const DB_NAME_MISSING  = 'No database name defined.';
    const HOST_MISSING     = 'No host defined.';
    const DBMS_MISSING     = 'No database management system provided.';
}
