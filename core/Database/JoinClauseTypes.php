<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 24/01/2017
 * Time: 23:39
 */

namespace Core\Database;

use Core\Enum\BasicEnum;

/**
 * Class JoinClauseTypes
 * @package Core\Database
 */
class JoinClauseTypes extends BasicEnum
{
    const CROSS = 'CROSS';
    const FULL = 'FULL';
    const INNER = 'INNER';
    const LEFT = 'LEFT';
    const NATURAL = 'NATURAL';
    const RIGHT = 'RIGHT';
}
