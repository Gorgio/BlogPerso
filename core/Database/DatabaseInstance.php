<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 16/12/2016
 * Time: 20:59
 */

namespace Core\Database;

use Core\Log\Logger;
use PDO;
use PDOException;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

/**
 * Class DatabaseInstance
 * @package Core\Database\Errors
 */
class DatabaseInstance
{
    use LoggerAwareTrait;

    private $dbHandler  = null;
    private $user       = null;
    private $password   = null;
    private $dbName     = null;
    private $host       = null;
    // DBMS = DataBase Management System.
    private $dbms       = null;

    /**
     * SQLDatabaseHandler constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
        if (is_null($logger) === true) {
            $logger = new Logger();
            $logger->setLoggingDir('database');
            $logger->setFileName('initiation');
        }
        $this->setLogger($logger);
    }

    /**
     * @return bool
     * @throws InvalidDbInstanceParamException
     */
    public function connect()
    {
        // Remove current instance of DBHandler if there is one.
        if (is_null($this->dbHandler) === false) {
            unset($this->dbHandler);
        }

        if (is_null($this->user()) === true) {
            throw new InvalidDbInstanceParamException(InvalidDbInstanceParamException::USER_MISSING);
        }
        if (is_null($this->password()) === true) {
            throw new InvalidDbInstanceParamException(InvalidDbInstanceParamException::PASSWORD_MISSING);
        }
        if (is_null($this->dbName()) === true) {
            throw new InvalidDbInstanceParamException(InvalidDbInstanceParamException::DB_NAME_MISSING);
        }
        if (is_null($this->host()) === true) {
            throw new InvalidDbInstanceParamException(InvalidDbInstanceParamException::HOST_MISSING);
        }
        if (is_null($this->dbms()) === true) {
            throw new InvalidDbInstanceParamException(InvalidDbInstanceParamException::DBMS_MISSING);
        }

        try {
            $dbConfig = [];
            $dbConfig['DBMS']      = $this->dbms();
            $dbConfig['HOST']      = $this->host();
            $dbConfig['DB_NAME']   = $this->dbName();
            $dbConfig['USER']      = $this->user();
            $dbConfig['PASSWORD']  = $this->password();
            $dbConfig['OPTIONS']   = [
                // PDO Exceptions.
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                // Change default fetch mode to FETCH_ASSOC (fetch() returns a associate array).
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ];

            $dbInstance = new PDO(
                $dbConfig['DBMS'] . ':host=' . $dbConfig['HOST'] .
                ';dbname=' . $dbConfig['DB_NAME'] . ';charset=utf8',
                $dbConfig['USER'],
                $dbConfig['PASSWORD'],
                $dbConfig['OPTIONS']
            );
            unset($dbConfig);

            $dbInstance->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

            $this->setDbHandler($dbInstance);
            return true;
        } catch (PDOException $e) {
            $this->logger->critical('DataBase connection failed : ' . $e->getMessage());
            return false;
        }
    }

    // $GETTERS & SETTERS.
    /**
     * @return null or PDO if defined
     */
    final public function getDbHandler()
    {
        return $this->dbHandler;
    }

    /**
     * @param PDO $dbHandler
     */
    final private function setDbHandler(PDO $dbHandler)
    {
        if ($dbHandler instanceof PDO) {
            $this->dbHandler = $dbHandler;
        }
    }

    /**
     * @return string
     */
    final private function dbms()
    {
        return $this->dbms;
    }

    /**
     * @param string $dbms
     */
    final public function setDbms($dbms)
    {
        if (DatabaseDbms::isValidValue($dbms) === true) {
            $this->dbms = $dbms;
        }
    }

    /**
     * @return string
     */
    final private function dbName()
    {
        return $this->dbName;
    }

    /**
     * @param string $dbName
     */
    public function setDbName(string $dbName)
    {
        $this->dbName = $dbName;
    }

    /**
     * @return string
     */
    final private function host()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host)
    {
        $this->host = $host;
    }

    /**
     * @return LoggerInterface
     */
    final protected function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return string
     */
    final private function password()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    final private function user()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user)
    {
        $this->user = $user;
    }
}
