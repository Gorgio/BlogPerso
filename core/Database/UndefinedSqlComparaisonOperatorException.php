<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/01/2017
 * Time: 14:11
 */

namespace Core\Database;

/**
 * Class InvalidComparaisonOperatorException
 * @package Core\Database
 */
class UndefinedSqlComparaisonOperatorException extends DatabaseException
{
    const GENERAL = 'Comparaison operator `{operator}` given is invalid or not supported.';
    const HAVING = 'Comparaison operator for HAVING clause: `{operator}` provided is invalid or unsupported.';
    const WHERE = 'Comparaison operator for WHERE clause: `{operator}` provided is invalid or unsupported.';

    /**
     * UndefinedSqlComparaisonOperatorException constructor.
     * @param string $message
     * @param string $operator
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message, string $operator, $code = 0, \Exception $previous = null)
    {
        $context = ['operator' => $operator];
        parent::__construct($message, $context, $code, $previous);
    }
}
