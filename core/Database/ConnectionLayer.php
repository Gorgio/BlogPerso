<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 16/12/2016
 * Time: 21:46
 */

namespace Core\Database;

/**
 * Class ConnectionLayer
 * @package Core\Database
 */
abstract class ConnectionLayer extends DatabaseInstance
{
    /**
     * @return mixed
     */
    abstract protected function setBasicReference();

    /**
     * @return mixed
     */
    abstract public function useRead();

    /**
     * @return mixed
     */
    abstract public function useCRU();

    /**
     * @return mixed
     */
    abstract public function useCRUD();
}
