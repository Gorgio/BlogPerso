<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 24/01/2017
 * Time: 23:59
 */

namespace Core\Database;

use Core\Enum\BasicEnum;
use PDO;

/**
 * Class ParamTypes
 * @package Core\Database
 */
class ParamTypes extends BasicEnum
{
    const BOOL = PDO::PARAM_BOOL;
    const INTEGER = PDO::PARAM_INT;
    const NULL = PDO::PARAM_NULL;
    const STRING = PDO::PARAM_STR;
}
