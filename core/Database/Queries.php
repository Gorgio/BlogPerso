<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 17/12/2016
 * Time: 17:44
 */

namespace Core\Database;

use Core\Traits\CoreFormatChecks;
use PDO;

/**
 * Class Queries
 * @package Core\Database
 */
class Queries extends QueryConstruct
{
    use CoreFormatChecks;

    private $nbRows = 0;

    /**
     * @param PDO $dbInstance
     * @param string $field
     * @return bool|true
     */
    final public function nbRows(PDO $dbInstance, $field = "")
    {
        if (empty($field) === true) {
            $field = 1;
        }

        $status = $this->addColumn($field, AggregateFunctionTypes::COUNT, 'nb_rows')->select($dbInstance);

        $this->singleOutResults();

        $results = $this->getResults();

        if (isset($results['nb_rows']) === false) {
            throw new \UnexpectedValueException('SQL Result is unexpected. NB Rows result does not exist.');
        }
        if (is_int($results['nb_rows']) === false) {
            throw new \UnexpectedValueException('SQL Result is unexpected. NB Rows isn\'t an integer.');
        }

        $this->setNbRows(intval($results['nb_rows']));

        $this->setResults(null);

        return $status;
    }

    /**
     * Check if the value wanted for field is available
     * @param PDO $dbInstance
     * @param string $field
     * @param $value
     * @param int $type
     * @param bool $cS Case Sensitive
     * @return bool
     */
    final public function isAvailable(PDO $dbInstance, string $field, $value, int $type = PDO::PARAM_STR, $cS = true)
    {
        $this->where($field, $value, $type, ComparaisonOperators::EQUAL, $cS)->nbRows($dbInstance);

        if ($this->getNbRows() === 0) {
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    final public function getNbRows(): int
    {
        return $this->nbRows;
    }

    /**
     * @param int $nbRows
     */
    final private function setNbRows(int $nbRows)
    {
        if ($this->isPositiveInteger($nbRows) === false) {
            throw new \InvalidArgumentException('Integer provided is negative.');
        }
        $this->nbRows = $nbRows;
    }
}
