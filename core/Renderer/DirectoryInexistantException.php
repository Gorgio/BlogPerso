<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 22/01/2017
 * Time: 16:16
 */

namespace Core\Renderer;

/**
 * Class DirectoryInexistantException
 * @package Core\Renderer
 */
class DirectoryInexistantException extends RenderException
{

}
