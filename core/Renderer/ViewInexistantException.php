<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 22/01/2017
 * Time: 16:04
 */

namespace Core\Renderer;

/**
 * Class ViewInexistantException
 * @package Core\Renderer
 */
class ViewInexistantException extends RenderException
{

}
