<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 22/01/2017
 * Time: 16:19
 */

namespace Core\Renderer;

/**
 * Class AbstractRenderer
 * @package Core\Renderer
 */
abstract class AbstractRenderer implements RendererInterface
{
    const DIR_COMPILATION_CACHE = 'temp' . DS . 'render' . DS;
    const DIR_VIEWS = ROOT . DS . 'views'. DS;
    const RENDER_VIEW_NONEXISTENT = 'View given doesn\'t exist.';

    protected $directory = '';
    protected $vars = [];

    /**
     * Render an HTML view
     *
     * @return mixed
     */
    abstract public function render();

    /**
     * @return string
     * @throws DirectoryInexistantException
     */
    public function getDirectory()
    {
        if (empty($this->directory) === true) {
            throw new DirectoryInexistantException();
        }
        return $this->directory;
    }

    /**
     * @param string $dir
     * @return void
     * @throws DirectoryInexistantException
     */
    public function setDirectory(string $dir)
    {
        if (is_dir($dir) === false) {
            throw new DirectoryInexistantException();
        }
        $this->directory = $dir;
    }

    /**
     * @return array
     */
    public function getVars(): array
    {
        return $this->vars;
    }

    /**
     * @param array $vars
     * @return void
     */
    public function setVars(array $vars)
    {
        $this->vars = array_merge($this->vars, $vars);
    }
}
