<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 30/07/2016
 * Time: 13:55
 */

namespace Core\Renderer;

use Exception;

/**
 * Class RenderException
 * @package Core\Renderer
 */
abstract class RenderException extends Exception
{

}
