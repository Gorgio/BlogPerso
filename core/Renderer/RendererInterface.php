<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 30/07/2016
 * Time: 13:48
 */

namespace Core\Renderer;

/**
 * Interface RendererInterface
 * @package Core\Renderer
 */
interface RendererInterface
{
    /**
     * Render an HTML view
     *
     * @return mixed
     */
    public function render();

    /**
     * @return array
     */
    public function getVars();

    /**
     * @param array $vars
     * @return void
     */
    public function setVars(array $vars);
}
