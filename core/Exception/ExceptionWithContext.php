<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 02/02/2017
 * Time: 09:59
 */

namespace Core\Exception;

use Exception;
use Core\Traits\Interpolate;

/**
 * Class ExceptionWithContext
 * @package Core\Exception
 */
class ExceptionWithContext extends Exception
{
    use Interpolate;

    /**
     * DatabaseException constructor.
     * @param string $message
     * @param array $context
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct(string $message, array $context = [], $code = 0, Exception $previous = null)
    {
        $message = $this->interpolate($message, $context);
        parent::__construct($message, $code, $previous);
    }
}
