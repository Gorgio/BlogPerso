<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 02/02/2017
 * Time: 09:57
 */

namespace Core\Package;

use Core\Exception\ExceptionWithContext;
use Exception;

/**
 * Class InaccessiblePropertyException
 * @package Core\Package
 */
class InaccessiblePropertyException extends ExceptionWithContext
{
    /**
     * InaccessiblePropertyException constructor.
     * @param string $property
     * @param string $class
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct(string $property, string $class, int $code = 0, Exception $previous = null)
    {
        $context = ['property' => $property, 'class' => $class];
        $message = 'No method to access property `{property}` in class `{class}` is available.';
        parent::__construct($message, $context, $code, $previous);
    }
}
