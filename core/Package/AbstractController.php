<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 13/08/2015
 * Time: 17:30
 */

namespace Core\Package;

use Core\Message\Message;
use Core\Message\MessageType;

/**
 * Class Controller
 * @package Core\Classes
 */
abstract class AbstractController
{
    use Message;

    const CSRF_POST_TOKEN_MISSING = 'POST CSRF token is missing';
    const CSRF_SESSION_TOKEN_MISSING = 'Session CSRF token is missing';
    const CSRF_TOKEN_MISMATCH = 'CSRF token don\'t correspond with one another.';

    /**
     * @param string|null $postToken
     * @param string|null $sessionToken
     * @return bool
     * @internal param array $sessions
     */
    protected function isCsrfProtectionPassed($postToken, $sessionToken)
    {
        $passed = true;

        if (is_null($postToken === true)) {
            $this->setMessage(self::CSRF_POST_TOKEN_MISSING, MessageType::APP_ERROR);
            $passed = false;
        }

        if (is_null($sessionToken === true)) {
            $this->setMessage(self::CSRF_SESSION_TOKEN_MISSING, MessageType::APP_ERROR);
            $passed = false;
        }

        if ($postToken !== $sessionToken) {
            $this->setMessage(self::CSRF_TOKEN_MISMATCH, MessageType::APP_ERROR);
            $passed = false;
        }

        return $passed;
    }
}
