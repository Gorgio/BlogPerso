<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/02/2016
 * Time: 15:07
 */

namespace Core\Package;

use Core\Log\Logger;
use Core\Message\Message;
use Core\Message\MessageType;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use ReflectionClass;

/**
 * Class BaseObject
 * @package Core\Classes
 */
abstract class AbstractObject
{
    use LoggerAwareTrait, Message;

    private $classShortName = '';

    /**
     * BaseObject constructor.
     * Init object if data is passed as a param
     *
     * @param array $objectData Data that will be passed to hydrate function
     * @param LoggerInterface $logger
     */
    public function __construct(array $objectData = [], LoggerInterface $logger = null)
    {
        $this->extractClassShortName();
        if (is_null($logger) === true) {
            $logger = new Logger();
        }
        $logger->setLoggingDir('object');
        $logger->setFileName($this->getClassShortName());
        $this->setLogger($logger);
        if (empty($objectData) === false) {
            $this->hydrate($objectData);
        }
    }

    /**
     * Hydrate Function
     * How does it work ?
     * First we get the called class name without it's Namespace
     * by using the ReflectionClass
     * A while loop loops though DataArray to obtain the correct method
     * name for the selected data (the Regexp)
     * Then check if the method exists and if so use the method to set the
     * corresponding value from the Data Array
     *
     * @param array $data Data to loop though
     *
     * @return void
     */
    public function hydrate(array $data)
    {
        $shortClassName = $this->getClassShortName();

        foreach ($data as $key => $value) {
            $keyName = preg_replace_callback(
                '#(' . $shortClassName . '_)?([a-z]*)(_?)([a-z]*)#',
                function ($matches) {
                    return $matches[2].ucfirst($matches[4]);
                },
                $key
            );
            $method = 'set'.ucfirst($keyName);

            if (method_exists($this, $method) === true) {
                $this->$method($value);
            }
        }
    }

    /**
     * @param array $properties
     * @return bool
     * @throws InaccessiblePropertyException
     * @throws InexistantPropertyException
     */
    public function checkData(array $properties)
    {
        $iteration = 0;
        foreach ($properties as $property) {
            if (property_exists($this, $property) === false) {
                throw new InexistantPropertyException($property, get_called_class());
            }
            if (method_exists($this, $property) === true) {
                $data = $this->$property();
            } elseif (method_exists($this, 'get'.ucfirst($property)) === true) {
                $method = 'get'.ucfirst($property);
                $data = $this->$method();
            } else {
                throw new InaccessiblePropertyException($property, get_called_class());
            }
            $iteration++;
            if (isset($data) !== true) {
                $this->setMessage(
                    '{property} provided isn\'t valid.',
                    MessageType::ERROR,
                    ['property' => $property]
                );
                $iteration--;
            }
        }

        if (count($properties) === $iteration) {
            return true;
        }
        return false;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    private function extractClassShortName()
    {
        $reflect = new ReflectionClass(get_called_class());
        $shortClassName = strtolower($reflect->getShortName());
        $this->classShortName = $shortClassName;
    }

    /**
     * @return string
     */
    public function getClassShortName(): string
    {
        return $this->classShortName;
    }
}
