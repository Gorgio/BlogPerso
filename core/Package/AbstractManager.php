<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 08/01/2017
 * Time: 11:30
 */

namespace Core\Package;

use Core\Database\Queries;
use Core\Message\MessageType;
use PDO;

/**
 * Class Manager
 * @package Core\Package
 */
abstract class AbstractManager extends Queries
{
    /**
     * @param PDO $dbInstance
     * @param array $properties
     * @param AbstractObject $object
     * @return bool
     */
    public function checkIsAvailable(PDO $dbInstance, array $properties, AbstractObject $object)
    {
        $iteration = 0;
        $isObjectCorrect = $object->checkData($properties);
        if ($isObjectCorrect === true) {
            foreach ($properties as $property) {
                $propertyMethod = 'get' . ucfirst($property);
                $isAvailable = $this->isAvailable($dbInstance, $property, $object->$propertyMethod());
                $iteration++;
                if ($isAvailable !== true) {
                    $this->setMessage(
                        ManagerMessages::PROPERTY_VALUE_UNAVAILABLE,
                        MessageType::ERROR,
                        ['property' => $property, 'value' => $object->$propertyMethod()]
                    );
                    $iteration--;
                }
            }
        }

        if (count($properties) === $iteration) {
            return true;
        }
        return false;
    }

    /**
     * @param PDO $dbInstance
     * @param array $properties
     * @param AbstractObject $object
     * @param AbstractObject $originalObject
     * @return bool
     */
    public function checkDifferenceIsAvailable(
        PDO $dbInstance,
        array $properties,
        AbstractObject $object,
        AbstractObject $originalObject
    ) {
        $isObjValid = $object->checkData($properties);
        $isOriginalObjValid = $originalObject->checkData($properties);
        $propertiesToCheck = [];

        if ($isObjValid !== true or $isOriginalObjValid !== true) {
            return false;
        }

        foreach ($properties as $property) {
            $propertyMethod = 'get' . ucfirst($property);
            if ($object->$propertyMethod() !== $originalObject->$propertyMethod()) {
                $propertiesToCheck[] = $property;
            }
        }

        if (empty($propertiesToCheck) === true) {
            return true;
        }

        return $this->checkIsAvailable($dbInstance, $propertiesToCheck, $object);
    }

    /**
     * @param PDO $dbInstance
     * @param AbstractObject $class
     * @param int $numberOfRows
     * @param int $offset
     * @return array
     */
    public function getListOf(PDO $dbInstance, AbstractObject $class, int $numberOfRows = 0, int $offset = 0)
    {
        if ($numberOfRows > 0) {
            $this->limit($numberOfRows, $offset);
        }
        $status = $this->select($dbInstance);
        if ($status === true) {
            $className = get_class($class);
            $objects = [];
            $data = $this->getResults();
            if (is_null($data) === false) {
                foreach ($data as $datum) {
                    $object = new $className($datum);
                    $objects[] = $object;
                }
            }
            $this->setResults($objects);
            return $objects;
        }
        return [];
    }

    /**
     * @param PDO $dbInstance
     * @param AbstractObject $object
     * @param int $page
     * @param int $elementsPerPage
     * @return array
     */
    final public function getListFromPage(PDO $dbInstance, AbstractObject $object, int $page, int $elementsPerPage)
    {
        $elementsToSelect = 0;
        $offset = 0;
        if ($this->isPositiveInteger($page) === true
            && $this->isPositiveInteger($elementsPerPage) === true
        ) {
            // FirstEntry is equal to offset.
            $firstEntry = (($page - 1) * $elementsPerPage);
            $elementsToSelect = $elementsPerPage;
            $offset = $firstEntry;
        }
        return $this->getListOf($dbInstance, $object, $elementsToSelect, $offset);
    }

    /**
     * @param PDO $dbInstance
     * @param $idValue
     * @param int $type
     * @return mixed
     */
    final public function getById(PDO $dbInstance, $idValue, int $type = PDO::PARAM_STR)
    {
        $this->where('id', $idValue, $type)->select($dbInstance);
        $this->singleOutResults();
        return $this->getResults();
    }

    /**
     * @param PDO $dbInstance
     * @param $idValue
     * @param int $type
     * @return bool
     */
    final public function delById(PDO $dbInstance, $idValue, int $type = PDO::PARAM_STR)
    {
        return $this->where('id', $idValue, $type)->deleteFrom($dbInstance);
    }
}
