<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 02/02/2017
 * Time: 10:05
 */

namespace Core\Package;

use Core\Exception\ExceptionWithContext;
use Exception;

/**
 * Class InexistantPropertyException
 * @package Core\Package
 */
class InexistantPropertyException extends ExceptionWithContext
{
    /**
     * InexistantPropertyException constructor.
     * @param string $property
     * @param string $class
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct(string $property, string $class, int $code = 0, Exception $previous = null)
    {
        $context = ['property' => $property, 'class' => $class];
        $message = 'No property `{property}` exist in class `{class}`.';
        parent::__construct($message, $context, $code, $previous);
    }
}
