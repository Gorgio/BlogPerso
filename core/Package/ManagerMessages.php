<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 11/01/2017
 * Time: 19:54
 */

namespace Core\Package;

/**
 * Class ManagerMessages
 * @package Core\Package
 */
class ManagerMessages
{
    const ADD_FAILURE = 'Error during adding process.';
    const ADD_SUCCESS = '{object} has been successfully added.';
    const DEL_FAILURE = 'Error during delete process.';
    const DEL_SUCCESS = '{object} has been successfully deleted.';
    const GET_LIST_FAILURE = 'Error while fetching list data for {object}.';
    const PROPERTY_VALUE_UNAVAILABLE = '{property} `{value}` is already taken.';
    const SET_FAILURE = 'Error during editing process.';
    const SET_INEXISTANT_ELEMENT = 'The {object} which is being edited is inexistant.';
    const SET_SUCCESS = '{object} has been successfully edited.';
}
