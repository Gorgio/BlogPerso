<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 03/04/2016
 * Time: 13:58
 */

namespace Core\Generate;

/**
 * Class Generate
 * Generates different specific tokens or strings.
 * @package Core\Classes
 */
class Generate
{
    /**
     * @param int $length
     * @return string
     */
    public static function randomAlphaNumString(int $length)
    {
        $characters = [
            'a','b','c','d','e','f','g','h','i','j','k','l','m', 'n', 'o','p','q','r','s','t','u','v','w','x','y','z',
            'A','B', 'C','D','E','F','G','H','I','J','K','L','M', 'N','O','P','Q', 'R','S','T','U','V','W','X','Y','Z',
            '1','2','3','4','5','6','7','8','9','0','-','_',
        ];
        $tableLastIndex = (count($characters) - 1);

        $string = '';
        if ($length > 0) {
            while ($length > 0) {
                $string .= $characters[random_int(0, $tableLastIndex)];
                $length --;
            }
        }
        return $string;
    }

    /**
     * Generate a secure CSRF (Cross Site Request Forgery) Token
     *
     * @return string
     */
    public static function csrfToken()
    {
        return bin2hex(openssl_random_pseudo_bytes(128));
    }
}
