<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/03/2016
 * Time: 20:37
 */

namespace Core\Redirection;

use Core\System\System;

/**
 * Class Redirect
 * @package Core\Redirection
 */
class Redirection
{
    /**
     * Redirect user to target page and handle a corresponding HTTP Status Code
     * @param string $target
     * @param int $statusCode
     */
    public static function redirect(string $target, int $statusCode = 303)
    {
        if (headers_sent() === false) {
            $target = System::$webRoot . $target . DS;
            header("Location: " . $target, true, $statusCode);
        }

        $serverVar = filter_input_array(INPUT_SERVER);
        $serverName = "http://" . $serverVar['SERVER_NAME'] . System::$webRoot;
        $urlTarget = $serverName;
        if (empty($target) === false) {
            $urlTarget .= $target;
        }

        ob_start();
        ?>
        <!DOCTYPE html>

        <html lang="en">
        <head>
            <!-- Meta -->
            <meta charset="utf-8">
            <meta name="description" content="Redirection Page">
            <meta name="robots" content="noindex, nofollow, noarchive, nocache, nosnippet, noodp">
            <script>
                window.location.href="<?php echo $urlTarget; ?>";
            </script>
            <noscript>
                <meta http-equiv="refresh" content="1; url=<?php echo $urlTarget; ?>">
            </noscript>
            <title>Redirection Page</title>
        </head>

        <body>
        <p>
            An error has occurred with the automatic redirection, please go to this
            <a href="<?php echo $urlTarget; ?>">Page</a> to continue your travel along the site.
        </p>
        </body>
        </html>
        <?php
        ob_end_flush();
    }
}