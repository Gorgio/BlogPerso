<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 10/01/2017
 * Time: 17:56
 */

namespace Core\System;

use Core\Patterns\Singleton;

/**
 * Class System
 * @package Core\System
 */
class System extends Singleton
{
    protected static $charset = 'UTF-8';
    private static $developmentEnvironment = false;
    protected static $language = null;
    public static $serverName;
    protected static $timezone = 'UTC';
    public static $webRoot = DS;


    /**
     * Config system.
     *
     * Sets encoding, error reporting, timezone, server name.
     */
    public static function config()
    {
        $serverVar = filter_input_array(INPUT_SERVER);
        $serverName = "://" . $serverVar['SERVER_NAME'] . self::$webRoot;
        self::setEncoding();
        self::setReporting();
        self::setServerName($serverName);
        self::setTimezone();
    }

    private static function setReporting()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 'Off');
        ini_set('log_errors', 'On');
        ini_set(
            'error_log',
            ROOT . DS . 'temp' . DS . 'logs' . DS . 'php.error.log'
        );
        if (self::$developmentEnvironment === true) {
            ini_set('display_errors', 'On');
        }
    }

    private static function setEncoding()
    {
        header('Content-Type: text/html; charset=' . self::$charset);

        mb_internal_encoding(self::$charset);
        mb_http_output(self::$charset);
        mb_http_input(self::$charset);
        mb_regex_encoding(self::$charset);
    }

    private static function setTimezone()
    {
        date_default_timezone_set(self::$timezone);
    }

    /**
     * @param boolean $devEnvironment
     */
    public static function setDevelopmentEnvironment(bool $devEnvironment)
    {
        self::$developmentEnvironment = $devEnvironment;
    }

    /**
     * @param string $serverName
     */
    public static function setServerName(string $serverName)
    {
        self::$serverName = $serverName;
    }

    /**
     * @return boolean
     */
    public static function isDevelopmentEnvironment(): bool
    {
        return self::$developmentEnvironment;
    }
}
