<?php
/**
 * Source : http://stackoverflow.com/questions/254514/php-and-enumerations
 */

namespace Core\Enum;

/**
 * Class BasicEnum
 * All child classes should be declared as abstract
 * @package Core\Enum
 */
abstract class BasicEnum
{
    private static $constCacheArray = null;

    /**
     * BasicEnum constructor to prevent instantiation.
     */
    final private function __construct()
    {
    }

    /**
     * To prevent cloning
     */
    final private function __clone()
    {
    }

    /**
     * @return mixed
     */
    private static function getConstants()
    {
        if (self::$constCacheArray === null) {
            self::$constCacheArray = [];
        }

        $calledClass = get_called_class();
        if (array_key_exists($calledClass, self::$constCacheArray) === false) {
            $reflect = new \ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }

        return self::$constCacheArray[$calledClass];
    }

    /**
     * @param $name
     * @param bool $strict
     *
     * @return bool
     */
    public static function isValidName($name, $strict = false)
    {
        $constants = self::getConstants();

        if ($strict === true) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    /**
     * @param mixed $value
     * @param bool  $strict
     *
     * @return bool
     */
    public static function isValidValue($value, $strict = true)
    {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict);
    }
}
