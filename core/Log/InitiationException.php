<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 23/10/2016
 * Time: 22:13
 */

namespace Core\Log;

/**
 * Class InitiationException
 *
 * @package Core\Log
 */
class InitiationException extends \Exception
{

}
