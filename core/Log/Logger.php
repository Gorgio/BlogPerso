<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 23/10/2016
 * Time: 21:17
 */

namespace Core\Log;

use Core\Traits\Interpolate;
use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;

/**
 * Class Logger
 * @package Core\Log
 * The basic version to Log events
 */
class Logger extends AbstractLogger
{
    use Interpolate;

    const BASE_LOGGING_DIR = ROOT . DS . 'temp' . DS . 'logs' . DS;

    const FAILED_TO_CREATE_DIR = 'Failed to create Logging Path Directory.';
    const FAILED_TO_WRITE_DIR = 'Impossible to write in given Directory.';

    protected $loggingDir   = self::BASE_LOGGING_DIR;
    protected $fileName     = 'default';

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     *
     * @throws \InvalidArgumentException
     */
    public function log($level, $message, array $context = [])
    {
        switch ($level) {
            case LogLevel::EMERGENCY:
                $message = 'EMERGENCY HAS BEEN FLAGGED [' . date('c') . '] ' . PHP_EOL . $message . PHP_EOL;
                break;
            case LogLevel::ALERT:
                $message = 'ALERT HAS BEEN FLAGGED [' . date('c') . '] ' . PHP_EOL . $message . PHP_EOL;
                break;
            case LogLevel::CRITICAL:
                $message = 'CRITICAL ERROR HAS BEEN FLAGGED [' . date('c') . '] ' . PHP_EOL . $message . PHP_EOL;
                break;
            case LogLevel::ERROR:
                $message = 'ERROR HAS BEEN FLAGGED [' . date('c') . '] ' . PHP_EOL . $message . PHP_EOL;
                break;
            case LogLevel::WARNING:
                $message = 'WARNING HAS BEEN FLAGGED [' . date('c') . '] ' . PHP_EOL . $message . PHP_EOL;
                break;
            case LogLevel::NOTICE:
                $message = 'NOTICE FLAGGED [' . date('c') . '] ' . PHP_EOL . $message . PHP_EOL;
                break;
            case LogLevel::INFO:
                break;
            case LogLevel::DEBUG:
                $message = 'DEBUG: [' . date('c') . '] ' . PHP_EOL . $message . PHP_EOL;
                break;
            default:
                throw new \InvalidArgumentException();
                break;
        }
        $message = $this->interpolate($message, $context);
        $this->write($message);
    }

    /**
     * @param $loggingDir
     * @return bool
     * @throws InitiationException
     */
    final protected function init($loggingDir)
    {
        if (file_exists($loggingDir) === false && is_dir($loggingDir) === false) {
            $status = mkdir($loggingDir, 0755, true);
            if ($status === false) {
                throw new InitiationException(self::FAILED_TO_CREATE_DIR);
            }
        }
        if (is_writable($loggingDir) === false) {
            throw new InitiationException(self::FAILED_TO_WRITE_DIR);
        }

        return true;
    }

    /**
     * Writes log to target path
     * @param string $message
     * @param string $fileName
     * @param string $loggingDir
     */
    protected function write(string $message, string $fileName = null, string $loggingDir = null)
    {
        if (is_null($fileName) === true) {
            $fileName = $this->getFileName();
        }
        if (is_null($loggingDir) === true) {
            $loggingDir = $this->getLoggingDir();
        }

        if ($this->init($loggingDir) === true) {
            $fileHandler = fopen($loggingDir. DS . $fileName, 'a');
            fwrite($fileHandler, $message . PHP_EOL);
            fclose($fileHandler);
        }
    }

    /**
     * @return string
     */
    public function getLoggingDir(): string
    {
        return $this->loggingDir;
    }

    /**
     * @param string $loggingDir
     */
    public function setLoggingDir(string $loggingDir)
    {
        $this->loggingDir = self::BASE_LOGGING_DIR . $loggingDir;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName)
    {
        $this->fileName = $fileName;
    }
}
