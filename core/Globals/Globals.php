<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 08/02/2017
 * Time: 18:38
 */

namespace Core\Globals;

use Core\Patterns\Singleton;

/**
 * Class Globals
 * @package Core\Globals
 */
abstract class Globals extends Singleton
{
    protected static $data = [];

    /**
     * Return array key value if defined otherwise null.
     *
     * @param string $key
     * @return mixed|null
     */
    public static function getKey(string $key)
    {
        if (key_exists($key, static::$data) === true) {
            return static::$data[$key];
        }
        return null;
    }

    /**
     * @return array
     */
    public static function getArray(): array
    {
        var_dump(static::$data);
        if (is_array(static::$data) === true) {
            return static::$data;
        }
        return [];
    }
}
