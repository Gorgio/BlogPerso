<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 01/02/2017
 * Time: 23:57
 */

namespace Core\Globals;

use Core\Patterns\Singleton;

/**
 * Class Get
 * @package Core\Globals
 */
final class Get extends Globals
{
    protected static $data = [];

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function init()
    {
        self::$data = filter_input_array(INPUT_GET);
        return parent::init();
    }
}
