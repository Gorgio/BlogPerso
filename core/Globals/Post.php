<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 01/02/2017
 * Time: 23:46
 */

namespace Core\Globals;

use Core\Patterns\Singleton;

/**
 * Class Post
 * @package Core
 */
final class Post extends Globals
{
    const CSRF_TOKEN_KEY = 'csrf';

    protected static $data = [];

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function init()
    {
        self::$data = filter_input_array(INPUT_POST);
        return parent::init();
    }
}
