<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 27/03/2016
 * Time: 11:20
 */

namespace Core\Globals;

use Core\Patterns\Singleton;

/**
 * Class Sessions
 * @package Core\Sessions
 */
class Session extends Singleton
{
    const SESSION_ID = 'id';

    const CSRF_TOKEN_KEY = 'csrf';

    /**
     * Initiate Sessions
     *
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function init()
    {
        static::startSessions();
        return parent::init();
    }

    /**
     *  Starts Session while checking params to prevent
     *  session fixation and hijacking
     * @internal
     */
    final private static function startSessions()
    {
        if (static::isActive() === false) {
            static::iniSetup();

            session_name(self::SESSION_ID);
            if (is_string(ini_get('session.use_cookies')) === true
                && isset($_COOKIE[self::SESSION_ID]) === false
            ) {
                $params = session_get_cookie_params();
                setcookie(
                    session_name(),
                    '',
                    $params['lifetime'],
                    $params['path'],
                    $params['domain'],
                    $params['secure'],
                    $params['httponly']
                );
            }
            session_start();
        }
    }

    /**
     *  Regenerates Session Id
     * @param bool $deleteOldSession
     */
    final public static function regenerateId($deleteOldSession = true)
    {
        session_regenerate_id($deleteOldSession);
    }

    /**
     * @return array
     */
    final public static function getArray()
    {
        return $_SESSION;
    }

    /**
     * @return bool
     * @internal
     */
    final private static function isActive()
    {
        if (session_status() === PHP_SESSION_NONE) {
            return false;
        }
        return true;
    }


    /**
     * @param $key
     * @return bool
     */
    final protected static function sessionExist($key)
    {
        if (isset(static::getArray()[$key]) === true) {
            return true;
        }
        return false;
    }

    /**
     * @param string $key
     * @param mixed $data
     * @return void
     */
    final public static function set(string $key, $data)
    {
        $_SESSION[$key] = $data;
    }

    /**
     * Get session key data
     * @param $key
     * @return mixed|null
     */
    final public static function getKey($key)
    {
        if (static::sessionExist($key) === true) {
            return static::getArray()[$key];
        }
        return null;
    }

    /**
     * Delete session key
     * @param $key
     */
    final public static function del($key)
    {
        if (static::sessionExist($key) === true) {
            unset($_SESSION[$key]);
        }
    }

    /**
     *  Destroys the current Session
     */
    final public static function destroy()
    {
        static::startSessions();
        // Unset all of the session variables.
        $_SESSION = [];

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (is_string(ini_get('session.use_cookies')) === true) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                (time() - 42000),
                $params['path'],
                $params['domain'],
                $params['secure'],
                $params['httponly']
            );
        }

        // Finally, destroy the session.
        session_destroy();
    }

    /**
     * Configure ini file for sessions.
     * @internal
     */
    final private static function iniSetup()
    {
        if (ini_get('session.use_trans_sid') !== 0) {
            ini_set('session.use_trans_sid', 0);
        }
        if (ini_get('session.use_only_cookies') !== 1) {
            ini_set('session.use_trans_sid', 1);
        }
        if (PHP_MAJOR_VERSION >= 7 && PHP_MINOR_VERSION >= 1) {
            if (ini_get('session.sid_length') < 64) {
                ini_set('session.sid_length', 64);
            }
            if (ini_get('session.sid_bits_per_character') < 5) {
                ini_set('session.sid_bits_per_character', 5);
            }
        } else {
            if (ini_get('session.hash_function') !== 'sha256'
                && ini_get('session.hash_function') !== 'sha512'
            ) {
                ini_set('session.hash_function', 'sha256');
            }
            if (ini_get('session.hash_bits_per_character') < 5) {
                ini_set('session.hash_bits_per_character', 5);
            }
            if (ini_get('session.entropy_file') === '') {
                ini_set('session.entropy_file', '/dev/urandom');
                ini_set('session.entropy_length', 256);
            }
        }
    }
}
