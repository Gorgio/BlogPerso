<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 06/04/2016
 * Time: 20:15
 */

namespace Core\Globals;

use Core\Patterns\Singleton;

/**
 * Class Cookies
 * @package Core\Classes
 */
final class Cookies extends Globals
{
    protected static $data = [];

    static protected $path = '';
    static protected $domain = '';
    static protected $secure = false;
    static protected $httpOnly = true;

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function init()
    {
        self::$data = filter_input_array(INPUT_COOKIE);
        return parent::init();
    }

    /**
     * @param string $identifier
     * @param string $value
     * @param int $expire
     */
    public static function set(string $identifier, string $value, int $expire)
    {
        setcookie(
            $identifier,
            $value,
            $expire,
            self::getPath(),
            self::getDomain(),
            self::isSecure(),
            self::isHttpOnly()
        );
    }

    /**
     * @param $key
     * @return void
     */
    public static function del($key)
    {
        setcookie($key, '', 1);
        setcookie($key, false);
        unset($_COOKIE[$key]);
    }

    // Configuration.
    /**
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httpOnly
     */
    public static function config(string $path = '', string $domain = '', bool $secure = false, bool $httpOnly = true)
    {
        self::setPath($path);
        self::setDomain($domain);
        self::setSecure($secure);
        self::setHttpOnly($httpOnly);
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return self::$path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        self::$path = $path;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return self::$domain;
    }

    /**
     * @param string $domain
     */
    public function setDomain(string $domain)
    {
        self::$domain = $domain;
    }

    /**
     * @return bool
     */
    public function isSecure()
    {
        return self::$secure;
    }

    /**
     * @param bool $secure
     */
    public function setSecure(bool $secure)
    {
        self::$secure = $secure;
    }

    /**
     * @return bool
     */
    public function isHttpOnly()
    {
        return self::$httpOnly;
    }

    /**
     * @param bool $httpOnly
     */
    public function setHttpOnly(bool $httpOnly)
    {
        self::$httpOnly = $httpOnly;
    }
}
