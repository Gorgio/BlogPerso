<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 17/02/2017
 * Time: 23:53
 */

namespace Core\Globals;

use Core\Patterns\Singleton;

/**
 * Class Server
 * @package Core\Globals
 */
class Server extends Globals
{
    protected static $data = [];

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function init()
    {
        self::$data = filter_input_array(INPUT_SERVER);
        return parent::init();
    }
}
