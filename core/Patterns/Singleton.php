<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 21/04/2016
 * Time: 20:14
 */

namespace Core\Patterns;

/**
 * Class Singleton
 * @package Core\Classes
 */
class Singleton
{
    /**
     * @var Singleton The reference to *Singleton* instance of this class
     */
    private static $instance;

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function init()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private method to prevent unserializing of the *Singleton* instance.
     *
     * @return void
     */
    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function __wakeup()
    {
    }
}
