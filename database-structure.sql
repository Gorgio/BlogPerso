-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Lun 24 Septembre 2018 à 18:53
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blogperso`
--
CREATE DATABASE IF NOT EXISTS `blogperso` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `blogperso`;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `title` tinytext NOT NULL,
  `content` text NOT NULL,
  `slug` tinytext NOT NULL,
  `category_id` tinyint(3) UNSIGNED NOT NULL,
  `tags` varchar(255) NOT NULL,
  `author_id` varchar(8) NOT NULL,
  `date_post` datetime NOT NULL,
  `date_edit` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `title`, `content`, `slug`, `category_id`, `tags`, `author_id`, `date_post`, `date_edit`, `deleted`) VALUES
(1, 'A Random entry', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem metus, suscipit ac mi quis, elementum mattis nulla. Aenean blandit auctor velit in rutrum. Quisque maximus vulputate quam eu ornare. Curabitur nec augue vitae est volutpat dictum at eu ante. Sed a purus facilisis, eleifend tortor nec, venenatis felis. Etiam lacinia lectus non lorem mattis laoreet a et erat. Integer eu placerat tortor. Curabitur nec ligula egestas, sodales nunc ac, dignissim lacus. Curabitur vel suscipit nulla. Pellentesque egestas nulla nisl, non aliquet nulla faucibus non. Proin nunc augue, tristique non bibendum eget, mattis id libero. Aliquam gravida augue at ipsum cursus posuere in at ligula.\n\nInteger mi augue, tempor ac scelerisque ac, pretium eget magna. Nam semper facilisis accumsan. Sed mollis, elit congue rutrum molestie, lorem felis porttitor leo, sit amet consequat sem odio eu mauris. Nam vitae orci id velit egestas dictum. Suspendisse ex ipsum, tincidunt a efficitur non, semper vitae dui. Ut convallis leo iaculis orci eleifend, sit amet tempus nunc elementum. Pellentesque posuere non lacus non interdum. Sed non accumsan massa. Cras vestibulum quis massa sit amet elementum. Sed sed diam turpis. Fusce at lacus turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat eget purus sit amet vulputate. Ut laoreet vestibulum mollis.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec est diam, aliquam vitae massa nec, pretium posuere urna. Curabitur bibendum sagittis lectus eu laoreet. Pellentesque aliquam hendrerit nisi, et cursus ipsum sagittis id. Vivamus a nisl sed sapien maximus lacinia vel fringilla nisi. Donec rhoncus dapibus massa sit amet pretium. Nullam vel orci facilisis ligula blandit tempor ac quis nisi. Aenean vitae vehicula nulla. Nunc vitae quam nibh. Aliquam eu libero felis. Etiam posuere libero augue, vitae lobortis risus viverra eget. Morbi euismod libero et velit fermentum, id pulvinar mauris accumsan. Suspendisse eu imperdiet ipsum, volutpat congue sapien. Sed congue porta rutrum. Vivamus quis mi condimentum, condimentum enim in, commodo ipsum.\n\nFusce pulvinar, orci ut tincidunt rhoncus, urna dui venenatis nulla, ut pharetra quam quam nec nisl. Donec ut tincidunt nibh. Donec lobortis eu felis sodales pulvinar. Donec tempor, erat vitae placerat dapibus, odio elit luctus urna, id mollis nibh lacus et nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam erat volutpat. Nulla scelerisque cursus turpis vel condimentum. Vivamus elementum egestas elit, vel malesuada nisl maximus vel. Nulla non malesuada felis. Nunc tincidunt volutpat nulla, a varius dui venenatis finibus. Morbi porttitor turpis ut ullamcorper dictum. Suspendisse leo turpis, tincidunt interdum augue at, aliquam ultricies dui. Donec nec tempor nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Vestibulum lorem eros, facilisis feugiat magna vel, faucibus gravida tellus.\n\nEtiam molestie ligula id turpis tempus ornare. Donec imperdiet, diam at ornare venenatis, nunc sem suscipit elit, rutrum bibendum nulla quam vitae ex. Aliquam at magna et urna mattis bibendum. Proin eget quam volutpat, tincidunt massa ut, cursus augue. Curabitur imperdiet at lectus ut maximus. Quisque vitae sapien ac augue dapibus dictum volutpat consequat odio. Donec non rutrum tortor, eu vulputate nisi.', 'random-entry', 1, '', 'George', '2016-02-27 19:30:00', NULL, 0),
(2, 'A second entry', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem metus, suscipit ac mi quis, elementum mattis nulla. Aenean blandit auctor velit in rutrum. Quisque maximus vulputate quam eu ornare. Curabitur nec augue vitae est volutpat dictum at eu ante. Sed a purus facilisis, eleifend tortor nec, venenatis felis. Etiam lacinia lectus non lorem mattis laoreet a et erat. Integer eu placerat tortor. Curabitur nec ligula egestas, sodales nunc ac, dignissim lacus. Curabitur vel suscipit nulla. Pellentesque egestas nulla nisl, non aliquet nulla faucibus non. Proin nunc augue, tristique non bibendum eget, mattis id libero. Aliquam gravida augue at ipsum cursus posuere in at ligula.\r\n\r\nInteger mi augue, tempor ac scelerisque ac, pretium eget magna. Nam semper facilisis accumsan. Sed mollis, elit congue rutrum molestie, lorem felis porttitor leo, sit amet consequat sem odio eu mauris. Nam vitae orci id velit egestas dictum. Suspendisse ex ipsum, tincidunt a efficitur non, semper vitae dui. Ut convallis leo iaculis orci eleifend, sit amet tempus nunc elementum. Pellentesque posuere non lacus non interdum. Sed non accumsan massa. Cras vestibulum quis massa sit amet elementum. Sed sed diam turpis. Fusce at lacus turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat eget purus sit amet vulputate. Ut laoreet vestibulum mollis.\r\n\r\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec est diam, aliquam vitae massa nec, pretium posuere urna. Curabitur bibendum sagittis lectus eu laoreet. Pellentesque aliquam hendrerit nisi, et cursus ipsum sagittis id. Vivamus a nisl sed sapien maximus lacinia vel fringilla nisi. Donec rhoncus dapibus massa sit amet pretium. Nullam vel orci facilisis ligula blandit tempor ac quis nisi. Aenean vitae vehicula nulla. Nunc vitae quam nibh. Aliquam eu libero felis. Etiam posuere libero augue, vitae lobortis risus viverra eget. Morbi euismod libero et velit fermentum, id pulvinar mauris accumsan. Suspendisse eu imperdiet ipsum, volutpat congue sapien. Sed congue porta rutrum. Vivamus quis mi condimentum, condimentum enim in, commodo ipsum.\r\n\r\nFusce pulvinar, orci ut tincidunt rhoncus, urna dui venenatis nulla, ut pharetra quam quam nec nisl. Donec ut tincidunt nibh. Donec lobortis eu felis sodales pulvinar. Donec tempor, erat vitae placerat dapibus, odio elit luctus urna, id mollis nibh lacus et nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam erat volutpat. Nulla scelerisque cursus turpis vel condimentum. Vivamus elementum egestas elit, vel malesuada nisl maximus vel. Nulla non malesuada felis. Nunc tincidunt volutpat nulla, a varius dui venenatis finibus. Morbi porttitor turpis ut ullamcorper dictum. Suspendisse leo turpis, tincidunt interdum augue at, aliquam ultricies dui. Donec nec tempor nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Vestibulum lorem eros, facilisis feugiat magna vel, faucibus gravida tellus.\r\n\r\nEtiam molestie ligula id turpis tempus ornare. Donec imperdiet, diam at ornare venenatis, nunc sem suscipit elit, rutrum bibendum nulla quam vitae ex. Aliquam at magna et urna mattis bibendum. Proin eget quam volutpat, tincidunt massa ut, cursus augue. Curabitur imperdiet at lectus ut maximus. Quisque vitae sapien ac augue dapibus dictum volutpat consequat odio. Donec non rutrum tortor, eu vulputate nisi.', 'second-entry', 1, '', 'George', '2016-02-28 16:20:00', NULL, 0),
(3, 'Third article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem metus, suscipit ac mi quis, elementum mattis nulla. Aenean blandit auctor velit in rutrum. Quisque maximus vulputate quam eu ornare. Curabitur nec augue vitae est volutpat dictum at eu ante. Sed a purus facilisis, eleifend tortor nec, venenatis felis. Etiam lacinia lectus non lorem mattis laoreet a et erat. Integer eu placerat tortor. Curabitur nec ligula egestas, sodales nunc ac, dignissim lacus. Curabitur vel suscipit nulla. Pellentesque egestas nulla nisl, non aliquet nulla faucibus non. Proin nunc augue, tristique non bibendum eget, mattis id libero. Aliquam gravida augue at ipsum cursus posuere in at ligula.\r\n\r\nInteger mi augue, tempor ac scelerisque ac, pretium eget magna. Nam semper facilisis accumsan. Sed mollis, elit congue rutrum molestie, lorem felis porttitor leo, sit amet consequat sem odio eu mauris. Nam vitae orci id velit egestas dictum. Suspendisse ex ipsum, tincidunt a efficitur non, semper vitae dui. Ut convallis leo iaculis orci eleifend, sit amet tempus nunc elementum. Pellentesque posuere non lacus non interdum. Sed non accumsan massa. Cras vestibulum quis massa sit amet elementum. Sed sed diam turpis. Fusce at lacus turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat eget purus sit amet vulputate. Ut laoreet vestibulum mollis.\r\n\r\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec est diam, aliquam vitae massa nec, pretium posuere urna. Curabitur bibendum sagittis lectus eu laoreet. Pellentesque aliquam hendrerit nisi, et cursus ipsum sagittis id. Vivamus a nisl sed sapien maximus lacinia vel fringilla nisi. Donec rhoncus dapibus massa sit amet pretium. Nullam vel orci facilisis ligula blandit tempor ac quis nisi. Aenean vitae vehicula nulla. Nunc vitae quam nibh. Aliquam eu libero felis. Etiam posuere libero augue, vitae lobortis risus viverra eget. Morbi euismod libero et velit fermentum, id pulvinar mauris accumsan. Suspendisse eu imperdiet ipsum, volutpat congue sapien. Sed congue porta rutrum. Vivamus quis mi condimentum, condimentum enim in, commodo ipsum.\r\n\r\nFusce pulvinar, orci ut tincidunt rhoncus, urna dui venenatis nulla, ut pharetra quam quam nec nisl. Donec ut tincidunt nibh. Donec lobortis eu felis sodales pulvinar. Donec tempor, erat vitae placerat dapibus, odio elit luctus urna, id mollis nibh lacus et nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam erat volutpat. Nulla scelerisque cursus turpis vel condimentum. Vivamus elementum egestas elit, vel malesuada nisl maximus vel. Nulla non malesuada felis. Nunc tincidunt volutpat nulla, a varius dui venenatis finibus. Morbi porttitor turpis ut ullamcorper dictum. Suspendisse leo turpis, tincidunt interdum augue at, aliquam ultricies dui. Donec nec tempor nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Vestibulum lorem eros, facilisis feugiat magna vel, faucibus gravida tellus.\r\n\r\nEtiam molestie ligula id turpis tempus ornare. Donec imperdiet, diam at ornare venenatis, nunc sem suscipit elit, rutrum bibendum nulla quam vitae ex. Aliquam at magna et urna mattis bibendum. Proin eget quam volutpat, tincidunt massa ut, cursus augue. Curabitur imperdiet at lectus ut maximus. Quisque vitae sapien ac augue dapibus dictum volutpat consequat odio. Donec non rutrum tortor, eu vulputate nisi.', 'third-article', 1, '', '', '2016-08-21 11:28:25', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(17, 'Hello'),
(1, 'Uncategorized'),
(16, 'Yolo');

-- --------------------------------------------------------

--
-- Structure de la table `contact_message`
--

DROP TABLE IF EXISTS `contact_message`;
CREATE TABLE `contact_message` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `name` varchar(255) NOT NULL COMMENT 'Name given to refer to during a response.',
  `mail` varchar(255) NOT NULL COMMENT 'E-mail to send an awnser back - if applicable.',
  `subject` varchar(255) NOT NULL COMMENT 'Subject of message',
  `content` text NOT NULL COMMENT 'Content of the message.',
  `date_send` datetime NOT NULL COMMENT 'Date at which the message was sent via the contact form.',
  `state` tinyint(2) UNSIGNED DEFAULT NULL COMMENT 'Status of the message.',
  `state_change_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `contact_message`
--

INSERT INTO `contact_message` (`id`, `name`, `mail`, `subject`, `content`, `date_send`, `state`, `state_change_date`) VALUES
(1, 'Gigi', 'gigi@gigi.com', 'Gigi is the Best', 'GIGI IS THE BEST', '2016-12-19 22:51:03', 0, '2016-12-22 22:50:03'),
(2, 'Test', 'test@test.test', 'Tro for', 'Olé lol je suis tro for', '2016-12-21 19:05:53', 0, '2016-12-22 22:50:10'),
(3, 'Member', 'member@member.com', 'Randomu', 'Just a random message.', '2016-12-22 21:47:35', 0, '2016-12-22 22:50:13'),
(4, 'Gigi', 'gigi@gigi.com', 'Test Contact', 'Olé', '2016-12-22 22:12:30', 0, NULL),
(5, 'Gigi', 'gigi@gigi.com', 'test after refactor', 'Yeah you know what I\'m doing', '2017-01-11 20:53:17', 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `rank`
--

DROP TABLE IF EXISTS `rank`;
CREATE TABLE `rank` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL,
  `privilege_level` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `rank`
--

INSERT INTO `rank` (`id`, `name`, `privilege_level`) VALUES
(1, 'Member', 1),
(3, 'Admin', 10);

-- --------------------------------------------------------

--
-- Structure de la table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `session_id` varchar(255) NOT NULL,
  `session_data` text NOT NULL,
  `session_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(8) NOT NULL,
  `date_register` date NOT NULL,
  `login` varchar(40) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rank_id` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `date_register`, `login`, `mail`, `name`, `password`, `rank_id`) VALUES
('-S2RFySB', '2016-10-19', 'member', 'member@member.com', 'Member', '$2y$12$7822RS6awzOhEmtvwTO8ie4oZaFvN/KR9JcoG85BtesGb.z06ELiW', 1),
('15Q8IYkM', '2017-01-07', 'test9', 'test9@test9.com', 'Test9', '$2y$12$Ly6/o9k18BbC3Nlw4IcrBueOMssgwazA9i11YCt6/ls4OHe2ttwMC', 1),
('2xfpssrT', '2016-12-18', 'test2', 'test2@test2.test', 'Test2', '$2y$12$niaPeWpWXxfSKsDoEYK/LOFcuxlRo1KOGzrOra2aFe/sLZ7YY4KkW', 1),
('gZmAETs6', '2017-01-10', 'test20', 'test20@test20.com', 'Test20', '$2y$12$OEmAhTlmSbV7NTjzRbNql.ofxQRRYL8kCg217sxqJa2FMrAGLuSQC', 1),
('J_Nmho0s', '2016-10-20', 'test5', 'test5@test5.test', 'Test5', '$2y$12$TMCM0lurUvpPI73YDzrdZu61Lv7reDv9hBosw1NvzguqUNpDdQgsy', 1),
('kOU_Kx2O', '2017-01-09', 'test6', 'test6@test6.com', 'Test6', '$2y$12$ZiHyI0gxP7W1FBmD2iIYK.Gb4Yxan94I4i7054JvhqOUawJW2zrgm', 1),
('lapzdXCL', '2016-09-05', 'admin', 'admin@admin.com', 'Admin', '$2y$12$8atGUmJ02WAmZY6VGXa.POIW1ylPx4FU19jGF11dtEZ6j76sbZoF2', 3),
('LkOO4Quv', '2017-01-10', 'Test21', 'test21@test21.com', 'Test21', '$2y$12$Cy.oN1.3bqNqMbCroUgXP.qQXpFL7Rl6FgJM8lQC1jBwqqr8SseTG', 1),
('SOVJbWsH', '2016-10-20', 'test', 'test@test.test', 'Test', '$2y$12$ii9juXakr7CPmtVwnp0H3.UsCCkBknHfbR0oZPjhHGuLUTVFNRYLq', 1),
('uCQaCADp', '2017-01-26', 'Test26', 'test26@test26.com', 'Test26', '$2y$12$eF8u8naouF8O4jLqmpGWS.Z6wxeeNGboHY5P0RxmmH8A1jbpSl93G', 1),
('VXi57U2r', '2016-12-18', 'Test8', 'test8@test8.test', 'Test8', '$2y$12$iF1z2a.yx3XnZ8y7edtNy.o2kE/6F0zuKuTupRY9sHTfiWrMMhOwi', 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`(255)),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `category_id_2` (`category_id`);

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `name_2` (`name`);

--
-- Index pour la table `contact_message`
--
ALTER TABLE `contact_message`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rank`
--
ALTER TABLE `rank`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rank_name` (`name`);

--
-- Index pour la table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`session_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD UNIQUE KEY `mail` (`mail`),
  ADD UNIQUE KEY `user_name` (`name`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `contact_message`
--
ALTER TABLE `contact_message`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `rank`
--
ALTER TABLE `rank`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
