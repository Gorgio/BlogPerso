-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 20 Septembre 2016 à 15:42
-- Version du serveur :  5.7.9
-- Version de PHP :  7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blogperso`
--
CREATE DATABASE IF NOT EXISTS `blogperso` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `blogperso`;

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` tinytext NOT NULL,
  `content` text NOT NULL,
  `slug` tinytext NOT NULL,
  `category_id` tinyint(3) UNSIGNED NOT NULL,
  `tags` varchar(255) NOT NULL,
  `author_id` varchar(8) NOT NULL,
  `date_post` datetime NOT NULL,
  `date_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`(255)),
  KEY `category_id` (`category_id`),
  KEY `category_id_2` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`id`, `title`, `content`, `slug`, `category_id`, `tags`, `author_id`, `date_post`, `date_edit`) VALUES
(1, 'A Random entry', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem metus, suscipit ac mi quis, elementum mattis nulla. Aenean blandit auctor velit in rutrum. Quisque maximus vulputate quam eu ornare. Curabitur nec augue vitae est volutpat dictum at eu ante. Sed a purus facilisis, eleifend tortor nec, venenatis felis. Etiam lacinia lectus non lorem mattis laoreet a et erat. Integer eu placerat tortor. Curabitur nec ligula egestas, sodales nunc ac, dignissim lacus. Curabitur vel suscipit nulla. Pellentesque egestas nulla nisl, non aliquet nulla faucibus non. Proin nunc augue, tristique non bibendum eget, mattis id libero. Aliquam gravida augue at ipsum cursus posuere in at ligula.\n\nInteger mi augue, tempor ac scelerisque ac, pretium eget magna. Nam semper facilisis accumsan. Sed mollis, elit congue rutrum molestie, lorem felis porttitor leo, sit amet consequat sem odio eu mauris. Nam vitae orci id velit egestas dictum. Suspendisse ex ipsum, tincidunt a efficitur non, semper vitae dui. Ut convallis leo iaculis orci eleifend, sit amet tempus nunc elementum. Pellentesque posuere non lacus non interdum. Sed non accumsan massa. Cras vestibulum quis massa sit amet elementum. Sed sed diam turpis. Fusce at lacus turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat eget purus sit amet vulputate. Ut laoreet vestibulum mollis.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec est diam, aliquam vitae massa nec, pretium posuere urna. Curabitur bibendum sagittis lectus eu laoreet. Pellentesque aliquam hendrerit nisi, et cursus ipsum sagittis id. Vivamus a nisl sed sapien maximus lacinia vel fringilla nisi. Donec rhoncus dapibus massa sit amet pretium. Nullam vel orci facilisis ligula blandit tempor ac quis nisi. Aenean vitae vehicula nulla. Nunc vitae quam nibh. Aliquam eu libero felis. Etiam posuere libero augue, vitae lobortis risus viverra eget. Morbi euismod libero et velit fermentum, id pulvinar mauris accumsan. Suspendisse eu imperdiet ipsum, volutpat congue sapien. Sed congue porta rutrum. Vivamus quis mi condimentum, condimentum enim in, commodo ipsum.\n\nFusce pulvinar, orci ut tincidunt rhoncus, urna dui venenatis nulla, ut pharetra quam quam nec nisl. Donec ut tincidunt nibh. Donec lobortis eu felis sodales pulvinar. Donec tempor, erat vitae placerat dapibus, odio elit luctus urna, id mollis nibh lacus et nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam erat volutpat. Nulla scelerisque cursus turpis vel condimentum. Vivamus elementum egestas elit, vel malesuada nisl maximus vel. Nulla non malesuada felis. Nunc tincidunt volutpat nulla, a varius dui venenatis finibus. Morbi porttitor turpis ut ullamcorper dictum. Suspendisse leo turpis, tincidunt interdum augue at, aliquam ultricies dui. Donec nec tempor nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Vestibulum lorem eros, facilisis feugiat magna vel, faucibus gravida tellus.\n\nEtiam molestie ligula id turpis tempus ornare. Donec imperdiet, diam at ornare venenatis, nunc sem suscipit elit, rutrum bibendum nulla quam vitae ex. Aliquam at magna et urna mattis bibendum. Proin eget quam volutpat, tincidunt massa ut, cursus augue. Curabitur imperdiet at lectus ut maximus. Quisque vitae sapien ac augue dapibus dictum volutpat consequat odio. Donec non rutrum tortor, eu vulputate nisi.', 'random-entry', 1, '', 'George', '2016-02-27 19:30:00', NULL),
(2, 'A second entry', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem metus, suscipit ac mi quis, elementum mattis nulla. Aenean blandit auctor velit in rutrum. Quisque maximus vulputate quam eu ornare. Curabitur nec augue vitae est volutpat dictum at eu ante. Sed a purus facilisis, eleifend tortor nec, venenatis felis. Etiam lacinia lectus non lorem mattis laoreet a et erat. Integer eu placerat tortor. Curabitur nec ligula egestas, sodales nunc ac, dignissim lacus. Curabitur vel suscipit nulla. Pellentesque egestas nulla nisl, non aliquet nulla faucibus non. Proin nunc augue, tristique non bibendum eget, mattis id libero. Aliquam gravida augue at ipsum cursus posuere in at ligula.\r\n\r\nInteger mi augue, tempor ac scelerisque ac, pretium eget magna. Nam semper facilisis accumsan. Sed mollis, elit congue rutrum molestie, lorem felis porttitor leo, sit amet consequat sem odio eu mauris. Nam vitae orci id velit egestas dictum. Suspendisse ex ipsum, tincidunt a efficitur non, semper vitae dui. Ut convallis leo iaculis orci eleifend, sit amet tempus nunc elementum. Pellentesque posuere non lacus non interdum. Sed non accumsan massa. Cras vestibulum quis massa sit amet elementum. Sed sed diam turpis. Fusce at lacus turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat eget purus sit amet vulputate. Ut laoreet vestibulum mollis.\r\n\r\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec est diam, aliquam vitae massa nec, pretium posuere urna. Curabitur bibendum sagittis lectus eu laoreet. Pellentesque aliquam hendrerit nisi, et cursus ipsum sagittis id. Vivamus a nisl sed sapien maximus lacinia vel fringilla nisi. Donec rhoncus dapibus massa sit amet pretium. Nullam vel orci facilisis ligula blandit tempor ac quis nisi. Aenean vitae vehicula nulla. Nunc vitae quam nibh. Aliquam eu libero felis. Etiam posuere libero augue, vitae lobortis risus viverra eget. Morbi euismod libero et velit fermentum, id pulvinar mauris accumsan. Suspendisse eu imperdiet ipsum, volutpat congue sapien. Sed congue porta rutrum. Vivamus quis mi condimentum, condimentum enim in, commodo ipsum.\r\n\r\nFusce pulvinar, orci ut tincidunt rhoncus, urna dui venenatis nulla, ut pharetra quam quam nec nisl. Donec ut tincidunt nibh. Donec lobortis eu felis sodales pulvinar. Donec tempor, erat vitae placerat dapibus, odio elit luctus urna, id mollis nibh lacus et nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam erat volutpat. Nulla scelerisque cursus turpis vel condimentum. Vivamus elementum egestas elit, vel malesuada nisl maximus vel. Nulla non malesuada felis. Nunc tincidunt volutpat nulla, a varius dui venenatis finibus. Morbi porttitor turpis ut ullamcorper dictum. Suspendisse leo turpis, tincidunt interdum augue at, aliquam ultricies dui. Donec nec tempor nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Vestibulum lorem eros, facilisis feugiat magna vel, faucibus gravida tellus.\r\n\r\nEtiam molestie ligula id turpis tempus ornare. Donec imperdiet, diam at ornare venenatis, nunc sem suscipit elit, rutrum bibendum nulla quam vitae ex. Aliquam at magna et urna mattis bibendum. Proin eget quam volutpat, tincidunt massa ut, cursus augue. Curabitur imperdiet at lectus ut maximus. Quisque vitae sapien ac augue dapibus dictum volutpat consequat odio. Donec non rutrum tortor, eu vulputate nisi.', 'second-entry', 1, '', 'George', '2016-02-28 16:20:00', NULL),
(3, 'Third article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem metus, suscipit ac mi quis, elementum mattis nulla. Aenean blandit auctor velit in rutrum. Quisque maximus vulputate quam eu ornare. Curabitur nec augue vitae est volutpat dictum at eu ante. Sed a purus facilisis, eleifend tortor nec, venenatis felis. Etiam lacinia lectus non lorem mattis laoreet a et erat. Integer eu placerat tortor. Curabitur nec ligula egestas, sodales nunc ac, dignissim lacus. Curabitur vel suscipit nulla. Pellentesque egestas nulla nisl, non aliquet nulla faucibus non. Proin nunc augue, tristique non bibendum eget, mattis id libero. Aliquam gravida augue at ipsum cursus posuere in at ligula.\r\n\r\nInteger mi augue, tempor ac scelerisque ac, pretium eget magna. Nam semper facilisis accumsan. Sed mollis, elit congue rutrum molestie, lorem felis porttitor leo, sit amet consequat sem odio eu mauris. Nam vitae orci id velit egestas dictum. Suspendisse ex ipsum, tincidunt a efficitur non, semper vitae dui. Ut convallis leo iaculis orci eleifend, sit amet tempus nunc elementum. Pellentesque posuere non lacus non interdum. Sed non accumsan massa. Cras vestibulum quis massa sit amet elementum. Sed sed diam turpis. Fusce at lacus turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat eget purus sit amet vulputate. Ut laoreet vestibulum mollis.\r\n\r\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec est diam, aliquam vitae massa nec, pretium posuere urna. Curabitur bibendum sagittis lectus eu laoreet. Pellentesque aliquam hendrerit nisi, et cursus ipsum sagittis id. Vivamus a nisl sed sapien maximus lacinia vel fringilla nisi. Donec rhoncus dapibus massa sit amet pretium. Nullam vel orci facilisis ligula blandit tempor ac quis nisi. Aenean vitae vehicula nulla. Nunc vitae quam nibh. Aliquam eu libero felis. Etiam posuere libero augue, vitae lobortis risus viverra eget. Morbi euismod libero et velit fermentum, id pulvinar mauris accumsan. Suspendisse eu imperdiet ipsum, volutpat congue sapien. Sed congue porta rutrum. Vivamus quis mi condimentum, condimentum enim in, commodo ipsum.\r\n\r\nFusce pulvinar, orci ut tincidunt rhoncus, urna dui venenatis nulla, ut pharetra quam quam nec nisl. Donec ut tincidunt nibh. Donec lobortis eu felis sodales pulvinar. Donec tempor, erat vitae placerat dapibus, odio elit luctus urna, id mollis nibh lacus et nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam erat volutpat. Nulla scelerisque cursus turpis vel condimentum. Vivamus elementum egestas elit, vel malesuada nisl maximus vel. Nulla non malesuada felis. Nunc tincidunt volutpat nulla, a varius dui venenatis finibus. Morbi porttitor turpis ut ullamcorper dictum. Suspendisse leo turpis, tincidunt interdum augue at, aliquam ultricies dui. Donec nec tempor nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Vestibulum lorem eros, facilisis feugiat magna vel, faucibus gravida tellus.\r\n\r\nEtiam molestie ligula id turpis tempus ornare. Donec imperdiet, diam at ornare venenatis, nunc sem suscipit elit, rutrum bibendum nulla quam vitae ex. Aliquam at magna et urna mattis bibendum. Proin eget quam volutpat, tincidunt massa ut, cursus augue. Curabitur imperdiet at lectus ut maximus. Quisque vitae sapien ac augue dapibus dictum volutpat consequat odio. Donec non rutrum tortor, eu vulputate nisi.', 'third-article', 1, '', '', '2016-08-21 11:28:25', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_name` varchar(40) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `name` (`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`) VALUES
(6, 'Little test'),
(4, 'Re re test'),
(1, 'Uncategorized'),
(12, 'Yele'),
(11, 'Yolo');

-- --------------------------------------------------------

--
-- Structure de la table `ranks`
--

DROP TABLE IF EXISTS `ranks`;
CREATE TABLE IF NOT EXISTS `ranks` (
  `rank_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rank_name` varchar(200) NOT NULL,
  `rank_privilege_level` tinyint(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`rank_id`),
  UNIQUE KEY `rank_name` (`rank_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `ranks`
--

INSERT INTO `ranks` (`rank_id`, `rank_name`, `rank_privilege_level`) VALUES
(1, 'Member', 1),
(3, 'Admin', 10);

-- --------------------------------------------------------

--
-- Structure de la table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(255) NOT NULL,
  `session_data` text NOT NULL,
  `session_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` varchar(8) NOT NULL,
  `user_login` varchar(40) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_rank_id` tinyint(3) UNSIGNED NOT NULL,
  `user_date_register` date NOT NULL,
  `user_mail` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login` (`user_login`),
  UNIQUE KEY `mail` (`user_mail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_password`, `user_rank_id`, `user_date_register`, `user_mail`) VALUES
('lapzdXCL', 'admin', '$2y$12$8atGUmJ02WAmZY6VGXa.POIW1ylPx4FU19jGF11dtEZ6j76sbZoF2', 3, '2016-09-05', 'admin@admin.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
